from __future__ import annotations

from itertools import chain
from typing import Union, List

from django.db import models
from django.db.models import Q, IntegerChoices
from django.db.models.deletion import CASCADE, SET_NULL, PROTECT
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from infra.models import Bed
from orea.models import BaseModel, BaseTextChoices


class Patient(BaseModel):
    class PatientSex(BaseTextChoices):
        man = "m", _("Man")
        woman = "w", _("Woman")
        undefined = "u", _("Undefined")

    local_id = models.CharField(max_length=10, unique=True, null=False)
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    birth_date = models.DateField(null=True)
    sex = models.CharField(choices=PatientSex.choices, null=False,
                           max_length=PatientSex.max_length(),
                           default=PatientSex.undefined)

    size_cm = models.IntegerField(null=True)
    weight_kg = models.FloatField(null=True)

    antecedents = models.JSONField(null=True)
    allergies = models.JSONField(null=True)

    @property
    def current_stay(self) -> PatientStay:
        return self.stays.filter(PatientStay.is_current_Q()).first()

    @property
    def bed_stays(self) -> List[BedStay]:
        return list(chain.from_iterable([
            list(s.bed_stays.all()) for s in self.stays.all()
        ]))

    @property
    def occupied_beds(self) -> List[Bed]:
        return [s.bed for s in self.bed_stays]

    class Meta:
        unique_together = (["local_id"],)

    def __str__(self):
        return f"{str(self.first_name).title()} " \
               f"{str(self.last_name).title()} ({str(self.local_id)})"


class PatientStay(BaseModel):
    class StaySeverity(IntegerChoices):
        danger = 0, _("With risks")
        unstable = 1, _("Unstable")
        stable = 2, _("Stable")

    created_by = models.ForeignKey(
        "orea.User", related_name='unit_stays_created',
        on_delete=SET_NULL, null=True)
    patient = models.ForeignKey(Patient, related_name='stays',
                                on_delete=CASCADE, null=False)
    hospitalisation_cause = models.TextField(null=True)
    severity = models.IntegerField(choices=StaySeverity.choices,
                                   default=StaySeverity.stable)

    @property
    def first_bed_stay(self) -> BedStay:
        return self.bed_stays.order_by("start_date").first()

    @property
    def current_bed_stay(self) -> BedStay:
        return self.bed_stays.filter(BedStay.is_current_Q()).first()

    @property
    def current_bed(self) -> Union[Bed, None]:
        return (self.current_bed_stay.bed
                if self.current_bed_stay is not None
                else None)

    @property
    def last_bed(self) -> Union[Bed, None]:
        if self.current_bed:
            return self.current_bed
        last_bed_stay = self.bed_stays.order_by('-end_date').first()
        return last_bed_stay.bed

    @property
    def start_date(self) -> timezone.datetime:
        first_stay = self.bed_stays.all().order_by("start_date").first()
        return first_stay.start_date if first_stay is not None else None

    @property
    def end_date(self) -> timezone.datetime:
        if self.bed_stays.filter(end_date__isnull=True).first() is not None:
            return None
        last_stay = self.bed_stays.all().order_by("-end_date").first()
        return last_stay.end_date if last_stay is not None else None

    @property
    def todo_list(self) -> str:
        return self.notes.all().filter(
            type=PatientNote.NoteType.todo_list).first()

    def __str__(self):
        date_format = '%d-%m-%Y'
        if self.is_finished:
            date_info = 'entre le ' + self.start_date.strftime(date_format) \
                        + ' et le ' + self.end_date.strftime(date_format)
        else:
            date_info = 'depuis le ' + self.start_date.strftime(date_format)
        return f"Patient {str(self.patient)}," \
               f"{str(self.last_bed.unit)} ({date_info})"

    @classmethod
    def is_current_Q(cls, prefix: str = "") -> Q:
        prefix = f"{prefix}__" if prefix else prefix
        return Q(**{
            f'{prefix}bed_stays__start_date__lte': timezone.now(),
            f'{prefix}bed_stays__end_date__isnull': True,
        })

    @property
    def is_finished(self):
        return self.current_bed_stay is None

    @property
    def bed_description(self):
        return str(self.current_bed)

    def close(self):
        if self.current_bed_stay is None:
            raise Exception(_("Cannot be closed"))
        self.current_bed_stay.close()

    def move_bed(self, new_bed: Bed):
        if self.current_bed_stay is None:
            raise Exception(_("Cannot move, stay has no current bed"))
        if str(self.current_bed.pk) == str(new_bed.pk):
            raise Exception(_("Cannot move, Destination bed cannot be the same"
                              " as current bed"))

        if new_bed.current_bed_stay is not None:
            other_patient_stay: PatientStay = (
                new_bed.current_bed_stay.patient_stay)

            new_bed.current_bed_stay.close()
            BedStay.objects.create(bed=self.current_bed,
                                   patient_stay=other_patient_stay)

        self.current_bed_stay.close()
        BedStay.objects.create(bed=new_bed, patient_stay=self)


class BedStay(BaseModel):
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(null=True)
    bed = models.ForeignKey("infra.Bed", related_name='bed_stays',
                            on_delete=PROTECT, null=False)
    patient_stay = models.ForeignKey(PatientStay, related_name="bed_stays",
                                     on_delete=PROTECT, null=False)

    @classmethod
    def is_current_Q(cls, prefix: str = "") -> Q:
        prefix = f"{prefix}_" if prefix else prefix
        return Q(**{
            f'{prefix}start_date__lte': timezone.now(),
            f'{prefix}end_date__isnull': True,
        })

    def close(self):
        self.end_date = timezone.now()
        self.save()


class PatientNote(BaseModel):
    created_by = models.ForeignKey(
        "orea.User", related_name='notes_created', on_delete=SET_NULL,
        null=True)
    stay = models.ForeignKey(PatientStay, related_name='notes',
                             on_delete=CASCADE, null=False)

    class NoteType(BaseTextChoices):
        recent_disease_history = "rdh",\
                                 _("recent disease history")
        evolution = "evolution", _("evolution")
        todo_list = "todo", _("todo list")
        treatment_limitations = "tl",\
                                _("treatment limitations")
        day_notice = "dn", _("day notice")
        general = "general", _("general")

    type = models.CharField(choices=NoteType.choices, default=NoteType.general,
                            max_length=NoteType.max_length())
    content = models.TextField()

    class Meta:
        unique_together = (("type", "stay", "created_by"))


class Measure(BaseModel):
    stay = models.ForeignKey(PatientStay, on_delete=CASCADE, null=False)
    created_by = models.ForeignKey(
        "orea.User", related_name='%(class)s_created', on_delete=SET_NULL,
        null=True)
    measure_datetime = models.DateTimeField(default=timezone.now, null=False)

    class Meta:
        abstract = True


class TreatmentLimitation(Measure):
    no_acr_reanimation = models.BooleanField(default=False)
    no_new_failures_or_therap_raise_treatment = models.BooleanField(
        default=False)
    no_catecholamines = models.BooleanField(default=False)
    no_intubation = models.BooleanField(default=False)
    no_assisted_ventilation = models.BooleanField(default=False)
    no_o2 = models.BooleanField(default=False)
    no_eer = models.BooleanField(default=False)
    no_transfusion = models.BooleanField(default=False)
    no_surgery = models.BooleanField(default=False)
    no_PIC_or_DVE = models.BooleanField(default=False)
    no_new_antibiotherapy = models.BooleanField(default=False)
    no_sirurgical_reintervetion = models.BooleanField(default=False)
    no_new_complementary_exams = models.BooleanField(default=False)
    no_biological_results = models.BooleanField(default=False)
    pressor_amines_stop = models.BooleanField(default=False)
    eer_stop = models.BooleanField(default=False)
    fio2_21percent = models.BooleanField(default=False)
    oxygenotherapy_stop = models.BooleanField(default=False)
    mecanic_ventilation_stop = models.BooleanField(default=False)
    extubation = models.BooleanField(default=False)
    nutrition_stop = models.BooleanField(default=False)
    hydratation_stop = models.BooleanField(default=False)
    antibiotic_stop = models.BooleanField(default=False)
    dve_ablation = models.BooleanField(default=False)
    ecmo_stop = models.BooleanField(default=False)
    all_current_therapeutics_stop = models.BooleanField(default=False)
    other_stops = models.TextField(default="", blank=True)
    fio2_limit = models.FloatField(null=True, default=None)
    no_mecanic_ventilation_markup = models.BooleanField(default=False)
    amines_limitation = models.FloatField(null=True, default=None)
    no_reanimation_admittance = models.BooleanField(default=False)
    treatment_limitations_comments = models.TextField(default="", blank=True)


class DetectionMeasure(Measure):
    blse = models.BooleanField(default=False)
    sarm = models.BooleanField(default=False)
    bhc = models.BooleanField(default=False)
    clostridium = models.BooleanField(default=False)

    class Meta:
        abstract = False


class FailureMeasure(Measure):
    class FailureType(BaseTextChoices):
        lung = "lung", _("lung")
        kidney = "kidney", _("kidney")
        heart = "heart", _("heart")
        metabolic = "metabolic", _("metabolic")
        brain = "brain", _("brain")
        liver = "liver", _("liver")
        hematologic = "hematologic", _("hematologic")

    failure_type = models.CharField(choices=FailureType.choices, null=False,
                                    max_length=FailureType.max_length())
    active = models.BooleanField(null=False)
    indication = models.TextField()

    class Meta:
        abstract = False


# class StatusMeasure(Measure):
#     status_type = models.IntegerField(choices=[
#         (0, "Ratio pression artérielle sur fraction inspirée"),
#         (1, "Noradrénaline"),
#         (2, "Créatinémie"),
#         (3, "Decubitus ventral"),
#         (4, "Mode ventilatoire"),
#         (5, "Sédation"),
#         (6, "Antibiotiques"),
#         (7, "Type de prélèvements"),
#         (8, "Germes"),
#         (9, "Curares"),
#         (10, "Lactatémie"),
#         (11, "Epuration extrarénale"),
#         (12, "Taux de prothrombine"),
#         (13, "Adrénaline"),
#         (14, "Dobutamine"),
#         (15, "Plaquettes"),
#         (16, "Glasgow"),
#         (17, "Débit de dioxygène inspiré"),
#         (18, "Fraction de dioxygène inspirée"),
#         (19, "Pression artérielle"),
#         (20, "Dépistage endo-réctal et otorhinolaryngologique"),
#     ], null=False)
#     value = models.TextField()
#
#     @property
#     def id_patient(self):
#         return self.patient.id
#
#     @property
#     def id_intensive_care_service(self):
#         return self.intensive_care_service.id
#
#     @property
#     # def id_unit_stay(self):
#     #     stays = PatientStay.objects.filter(
#     #         patient=self.patient,
#     #         start_date__lte=self.created_date,
#     #     )
#     #     stays = (stays.filter(end_date__gte=self.created_date)
#     #              | stays.filter(end_date__isnull=True))
#     #     stay = stays.first()
#     #     if stay is not None:
#     #         return stay.id
#     #     else:
#     #         return None
#
#     class Meta:
#         abstract = False
