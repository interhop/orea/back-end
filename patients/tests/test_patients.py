from __future__ import annotations

import random
from datetime import timedelta
from random import randint
from typing import List, Union

from django.utils import timezone
from django.utils.datetime_safe import date
from rest_framework import status

from infra.tests.tools import ViewSetTestsWithBasicInfra
from infra.models import Bed
from orea.models import User
from orea.tests_tools import CreateCase, CaseRetrieveFilter, \
    new_random_user, PatchCase, DeleteCase, random_str, ListCase
from patients.models import Patient, PatientStay, BedStay
from patients.views import PatientViewSet
from users.models import Access

PATIENTS_URL = "/patients/"


class PatientCaseRetrieveFilter(CaseRetrieveFilter):
    def __init__(self, local_id: str, exclude: dict = None):
        self.local_id = local_id
        super(PatientCaseRetrieveFilter, self).__init__(exclude=exclude)


class PatientCase:
    def __init__(self, bed: Bed, initial_data: dict, current_stay: bool = True):
        self.bed = bed
        self.initial_data = initial_data
        self.current_stay = current_stay


class PatientPatchCase(PatchCase, PatientCase):
    def __init__(self, bed: Bed, **kwargs):
        super(PatientPatchCase, self).__init__(**kwargs)
        PatientCase.__init__(self, bed, self.initial_data)


class PatientDeleteCase(DeleteCase, PatientCase):
    def __init__(self, bed: Union[Bed, None],
                 current_stay: bool = True, **kwargs):
        super(PatientDeleteCase, self).__init__(**kwargs)
        PatientCase.__init__(self, bed, self.data_to_delete, current_stay)


# TESTS ########################################################################

class PatientsTests(ViewSetTestsWithBasicInfra):
    unupdatable_fields = []
    unsettable_default_fields = dict()
    unsettable_fields = []

    objects_url = f"{PATIENTS_URL}/patients"
    retrieve_view = PatientViewSet.as_view({'get': 'retrieve'})
    list_view = PatientViewSet.as_view({'get': 'list'})
    create_view = PatientViewSet.as_view({'post': 'create'})
    delete_view = PatientViewSet.as_view({'delete': 'destroy'})
    update_view = PatientViewSet.as_view({'patch': 'partial_update'})
    model = Patient
    model_objects = Patient.objects
    model_fields = Patient._meta.fields

    def init_case_obj(self, case: PatientCase):
        obj_id = self.model_objects.create(**case.initial_data).pk
        obj = self.model_objects.get(pk=obj_id)

        patient_stay: PatientStay = PatientStay.objects.create(
            created_by=self.user_1,
            patient=obj
        )
        BedStay.objects.create(
            start_date=timezone.now() - timedelta(days=2),
            end_date=((timezone.now() - timedelta(days=1))
                      if not case.current_stay else None),
            bed=case.bed,
            patient_stay=patient_stay,
        )
        return obj

    def setUp(self):
        super(PatientsTests, self).setUp()
        self.user_1: User = new_random_user("user", "1")
        self.access_user_1_serv_1: Access = Access.objects.create(
            intensive_care_service=self.serv_1,
            user=self.user_1,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=False,
            right_review=True
        )


class CreatePatientsTests(PatientsTests):
    def setUp(self):
        super(CreatePatientsTests, self).setUp()
        test_local_id = "super_id"

        self.patient: Patient = Patient.objects.create(
            local_id='patient_1',
            first_name='pati',
            last_name='one',
            birth_date=date.fromisoformat("2000-01-02"),
            sex=Patient.PatientSex.man,
            size_cm=160,
            weight_kg=60,
            antecedents="{}",
            allergies="{}",
        )
        self.patient_stay: PatientStay = PatientStay.objects.create(
            created_by=self.user_1,
            patient=self.patient,
            hospitalisation_cause="",
            severity=PatientStay.StaySeverity.stable,
        )
        self.patient_bed_stay: BedStay = BedStay.objects.create(
            start_date=timezone.now() - timedelta(days=1),
            bed=self.hosp_1_beds[0],
            patient_stay=self.patient_stay,
        )

        self.creation_data = {
            'local_id': test_local_id,
            'first_name': 'test',
            'last_name': 'test',
            'birth_date': date.fromisoformat("2000-01-01"),
            'sex': Patient.PatientSex.undefined,
            'size_cm': 150,
            'weight_kg': 50,
            'antecedents': "{}",
            'allergies': "{}",
        }
        self.basic_create_case = CreateCase(
            data=self.creation_data,
            retrieve_filter=PatientCaseRetrieveFilter(local_id=test_local_id),
            user=None, status=None, success=None,
        )

    def test_create_patient(self):
        # As a user, I can create a patient
        case = self.basic_create_case.clone(
            user=self.user_1,
            status=status.HTTP_201_CREATED,
            success=True,
        )
        self.check_create_case(case)

    def test_create_patient_even_without_service(self):
        # As a user without access, I can create a patient
        self.access_user_1_serv_1.delete()
        case = self.basic_create_case.clone(
            user=self.user_1,
            status=status.HTTP_201_CREATED,
            success=True,
        )
        self.check_create_case(case)

    def test_err_create_patient_wrong_data(self):
        # As a user, I cannot create a patient with some wrong values
        cases = [self.basic_create_case.clone(
            data={**self.creation_data, k: v},
            user=self.user_1,
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
        ) for k, v in [
            # todo : complete
            ('birth_date', timezone.now() + timedelta(days=1)),
            ('sex', "x"),
            # ('antecedents', "{"),
            # ('allergies', "{"),
            ('size_cm', -150),
            ('weight_kg', -50)
        ]]
        [self.check_create_case(case) for case in cases]

    def test_err_create_patient_with_current_stay(self):
        # As a user, I can create a patient with local_id of a patient
        # that is in a current stay
        case = self.basic_create_case.clone(
            data={**self.creation_data, 'local_id': self.patient.local_id},
            user=self.user_1,
            status=status.HTTP_409_CONFLICT,
            success=False,
        )

        self.check_create_case(case)

    def test_create_patient_existing(self):
        # As a user, I can create a patient with local_id existing,
        # it will update it
        self.patient_bed_stay.delete()
        self.patient_stay.delete()
        case = self.basic_create_case.clone(
            data={**self.creation_data, 'local_id': self.patient.local_id},
            user=self.user_1,
            status=status.HTTP_201_CREATED,
            success=False,
        )

        self.check_create_case(case)


class PatientGetListTests(PatientsTests):
    def setUp(self):
        super(PatientGetListTests, self).setUp()
        self.name_pattern = random_str(3).replace(' ', 'e')
        self.test_date = date.fromisoformat("2000-01-01")
        self.test_int = 100
        nb_patients = 500

        self.admin_user: User = new_random_user("admin", "main", is_staff=True)

        def random_str_value(perfect_match_possible: bool = False):
            return random_str(
                randint(5, 7),
                include_pattern=(self.name_pattern
                                 if random.random() > 0.8 else "")
            ) if (not perfect_match_possible or random.random()) > 0.2\
                else self.name_pattern

        self.list_patients: List[Patient] = Patient.objects.bulk_create([
            Patient(
                local_id=random_str_value(),
                first_name=random_str_value(perfect_match_possible=True),
                last_name=random_str_value(perfect_match_possible=True),
                birth_date=self.test_date + timedelta(days=randint(0, 5)),
                sex=random.choice(Patient.PatientSex.values),
                size_cm=self.test_int + randint(0, 10),
                weight_kg=self.test_int + randint(0, 10),
                antecedents='{"key": "' + random_str_value() + '"}',
                allergies='{"key": "' + random_str_value() + '"}',
            ) for _ in range(nb_patients)
        ])
        list_patient_stays: List[PatientStay] = PatientStay.objects.bulk_create(
            [PatientStay(
                created_by=self.user_1,
                patient=p
            ) for p in self.list_patients]
        )
        BedStay.objects.bulk_create(
            [BedStay(
                start_date=timezone.now() - timedelta(days=2),
                end_date=(None if random.random() > 0.8
                          else timezone.now() - timedelta(days=1)),
                patient_stay=ps,
                bed=random.choice(self.hosp_1_beds + self.hosp_2_beds)
            ) for ps in list_patient_stays]
        )

        self.basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                                    user=self.admin_user)

    def test_get_all_patients_as_admin(self):
        # As a user with admin right, I can get all patients
        case = ListCase(
            to_find=self.list_patients,
            success=True,
            status=status.HTTP_200_OK,
            user=self.admin_user
        )
        self.check_get_paged_list_case(case)

    def test_get_all_patients_from_my_service(self):
        # As a user with admin right, I can get all patients
        case = ListCase(
            to_find=[p for p in self.list_patients
                     if any(b in self.hosp_1_beds for b in p.occupied_beds)],
            success=True,
            status=status.HTTP_200_OK,
            user=self.user_1
        )
        self.check_get_paged_list_case(case)

    def test_get_list_with_search_param(self):
        # As an admin user, I can get patients filtered with search filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"search={self.name_pattern.lower()}",
            to_find=[
                p for p in self.list_patients
                if any(self.name_pattern.lower() in getattr(p, f).lower()
                       for f in ["local_id", "first_name", "last_name",
                                 "antecedents", "allergies"])
            ],
            params=dict(search=self.name_pattern)
        ))

    def test_get_list_with_first_name_param(self):
        # As an admin user, I can get patients filtered with first_name filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"first_name={self.name_pattern}",
            to_find=[
                p for p in self.list_patients
                if self.name_pattern == p.first_name
            ],
            params=dict(first_name=self.name_pattern)
        ))

    def test_get_list_with_last_name_param(self):
        # As an admin user, I can get patients filtered with last_name filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"last_name={self.name_pattern}",
            to_find=[
                p for p in self.list_patients
                if self.name_pattern == p.last_name
            ],
            params=dict(last_name=self.name_pattern)
        ))

    def test_get_list_with_birth_date_param(self):
        # As an admin user, I can get patients filtered with birth_date filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"birth_date={self.test_date}",
            to_find=[
                p for p in self.list_patients
                if self.test_date == p.birth_date
            ],
            params=dict(birth_date=self.test_date)
        ))

    def test_get_list_with_sex_param(self):
        # As an admin user, I can get patients filtered with sex filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"sex={Patient.PatientSex.undefined.value}",
            to_find=[
                p for p in self.list_patients
                if Patient.PatientSex.undefined.value == p.sex
            ],
            params=dict(sex=Patient.PatientSex.undefined.value)
        ))

    def test_get_list_with_size_cm_param(self):
        # As an admin user, I can get patients filtered with size_cm filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"size_cm={self.test_int}",
            to_find=[
                p for p in self.list_patients
                if self.test_int == p.size_cm
            ],
            params=dict(size_cm=self.test_int)
        ))

    def test_get_list_with_weight_kg_param(self):
        # As an admin user, I can get patients filtered with weight_kg filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"weight_kg={self.test_int}",
            to_find=[
                p for p in self.list_patients
                if self.test_int == p.weight_kg
            ],
            params=dict(weight_kg=self.test_int)
        ))

    def test_error_not_authenticated(self):
        # As a user not authenticated, I cannot get any patients
        case = ListCase(
            user=None,
            to_find=[],
            success=False,
            status=status.HTTP_401_UNAUTHORIZED
        )
        self.check_get_paged_list_case(case)


class PatchPatientsTests(PatientsTests):
    def check_patch_case(self, case: PatientPatchCase):
        obj = self.init_case_obj(case)
        super(PatchPatientsTests, self).check_patch_case(case, obj)

    def setUp(self):
        super().setUp()
        test_local_id = "test_id"

        self.creation_data = {
            'local_id': test_local_id,
            'first_name': 'test',
            'last_name': 'test',
            'birth_date': date.fromisoformat("2000-01-01"),
            'sex': Patient.PatientSex.undefined,
            'size_cm': 150,
            'weight_kg': 50,
            'antecedents': "{}",
            'allergies': "{}",
        }

        self.basic_patch_case = PatientPatchCase(
            bed=self.hosp_1_beds[0],
            initial_data=self.creation_data,
            data_to_update=dict(
                first_name='test',
                last_name='test',
                birth_date=date.fromisoformat("2000-01-01"),
                sex=Patient.PatientSex.undefined,
                size_cm=150,
                weight_kg=50,
                antecedents="{}",
                allergies="{}",
            ),
            user=self.user_1,
            success=True,
            status=status.HTTP_200_OK,
        )

    def test_patch_patient(self):
        # As a user with access to a service, I can patch a patient
        # staying in this service
        self.check_patch_case(self.basic_patch_case)

    def test_err_patch_patient_other_service(self):
        # As a user, I cannot patch a patient
        # staying in a service I don't have access to
        case = self.basic_patch_case.clone(
            bed=self.hosp_2_beds[0],
            success=False,
            status=status.HTTP_404_NOT_FOUND
        )
        self.check_patch_case(case)


class DeletePatientsTests(PatientsTests):
    def check_delete_case(self, case: PatientDeleteCase):
        obj = self.init_case_obj(case) if case.bed else None
        super(DeletePatientsTests, self).check_delete_case(case, obj)

    def setUp(self):
        super().setUp()
        test_local_id = "test_id"
        self.admin_user: User = new_random_user("admin", "main", is_staff=True)

        self.basic_delete_case = PatientDeleteCase(
            bed=None,
            data_to_delete={
                'local_id': test_local_id,
                'first_name': 'test',
                'last_name': 'test',
                'birth_date': date.fromisoformat("2000-01-01"),
                'sex': Patient.PatientSex.undefined,
                'size_cm': 150,
                'weight_kg': 50,
                'antecedents': "{}",
                'allergies': "{}",
            },
            user=self.admin_user,
            success=True,
            status=status.HTTP_204_NO_CONTENT,
        )

    def test_delete_patient_with_no_stay_as_admin(self):
        # As an admin user, I can delete a patient
        # with no current stay
        self.check_delete_case(self.basic_delete_case)

    def test_err_delete_patient_as_not_admin(self):
        # As a user, I cannot delete a patient
        self.check_delete_case(self.basic_delete_case.clone(
            user=self.user_1,
            success=False,
            status=status.HTTP_404_NOT_FOUND,
        ))

    def test_err_delete_patient_with_current_stay(self):
        # As a user, I cannot delete a patient with a current stay
        self.check_delete_case(self.basic_delete_case.clone(
            bed=self.hosp_1_beds[0],
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
        ))

    def test_err_delete_patient_with_current_stay_not_as_admin(self):
        # As an admin user, I cannot delete a patient with a current stay
        self.check_delete_case(self.basic_delete_case.clone(
            user=self.user_1,
            bed=self.hosp_1_beds[0],
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        ))

    def test_err_delete_patient_unauthenticated(self):
        # As a user not authenticated, I cannot delete a patient
        self.check_delete_case(self.basic_delete_case.clone(
            user=None,
            success=False,
            status=status.HTTP_401_UNAUTHORIZED,
        ))
