from __future__ import annotations

import datetime
import random
from datetime import timedelta
from random import randint
from typing import List

from django.utils import timezone
from django.utils.datetime_safe import date
from rest_framework import status

from infra.tests.tools import ViewSetTestsWithBasicInfra
from infra.models import Bed
from orea.models import User
from orea.tests_tools import CreateCase, CaseRetrieveFilter, \
    new_random_user, PatchCase, DeleteCase, random_str, ListCase
from patients.models import Patient, PatientStay, BedStay,\
    TreatmentLimitation, DetectionMeasure, FailureMeasure, PatientNote
from patients.views import TreatmentLimitationViewSet, \
    DetectionMeasureViewSet, FailureMeasureViewSet, PatientNoteViewSet
from users.models import Access

PATIENTS_URL = "/patients/"


class MeasureCaseRetrieveFilter(CaseRetrieveFilter):
    def __init__(self, measure_datetime: datetime, exclude: dict = None):
        self.measure_datetime = measure_datetime
        super(MeasureCaseRetrieveFilter, self).__init__(exclude=exclude)


class PatientNoteCaseRetrieveFilter(CaseRetrieveFilter):
    def __init__(self, content: str, exclude: dict = None):
        self.content = content
        super(PatientNoteCaseRetrieveFilter, self).__init__(exclude=exclude)


class MeasureCase:
    def __init__(self, bed: Bed, initial_data: dict, current_stay: bool = True):
        self.bed = bed
        self.initial_data = initial_data
        self.current_stay = current_stay


class MeasurePatchCase(PatchCase, MeasureCase):
    def __init__(self, bed: Bed, **kwargs):
        super(MeasurePatchCase, self).__init__(**kwargs)
        MeasureCase.__init__(self, bed, self.initial_data)


class MeasureDeleteCase(DeleteCase, MeasureCase):
    def __init__(self, bed: Bed, current_stay: bool = True, **kwargs):
        super(MeasureDeleteCase, self).__init__(**kwargs)
        MeasureCase.__init__(self, bed, self.data_to_delete, current_stay)


# TESTS ####################################################################

# BASE

class MeasureTests(ViewSetTestsWithBasicInfra):
    def setUp(self):
        super(MeasureTests, self).setUp()
        self.admin_user: User = new_random_user("admin", "user", is_staff=True)
        self.user_1: User = new_random_user("user", "1")
        self.user_2: User = new_random_user("user", "2")
        self.access_user_1_serv_1: Access = Access.objects.create(
            intensive_care_service=self.serv_1,
            user=self.user_1,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=False,
            right_review=True
        )

        self.patient_1: Patient = Patient.objects.create(local_id="pat_1")
        self.stay_1: PatientStay = PatientStay.objects.create(
            patient=self.patient_1,
            created_by=self.user_1
        )
        self.bed_stay_1: BedStay = BedStay.objects.create(
            bed=self.hosp_1_beds[0],
            patient_stay=self.stay_1,
            start_date=timezone.now() - timedelta(days=1)
        )

        self.patient_2: Patient = Patient.objects.create(local_id="pat_2")
        self.stay_2: PatientStay = PatientStay.objects.create(
            patient=self.patient_2,
            created_by=self.user_2
        )
        self.bed_stay_2: BedStay = BedStay.objects.create(
            bed=self.hosp_2_beds[0],
            patient_stay=self.stay_2,
            start_date=timezone.now() - timedelta(days=1)
        )

        self.patient_left: Patient = Patient.objects.create(local_id="pat_left")
        self.stay_left: PatientStay = PatientStay.objects.create(
            patient=self.patient_left,
            created_by=self.user_1
        )
        self.bed_stay_left: BedStay = BedStay.objects.create(
            bed=self.hosp_1_beds[1],
            patient_stay=self.stay_left,
            start_date=timezone.now() - timedelta(days=2),
            end_date=timezone.now() - timedelta(days=1),
        )


class TreatmentLimitationTests(MeasureTests):
    unupdatable_fields = []
    unsettable_default_fields = dict()
    unsettable_fields = []

    objects_url = f"{PATIENTS_URL}/treatment-limitations"
    retrieve_view = TreatmentLimitationViewSet.as_view({'get': 'retrieve'})
    list_view = TreatmentLimitationViewSet.as_view({'get': 'list'})
    create_view = TreatmentLimitationViewSet.as_view({'post': 'create'})
    delete_view = TreatmentLimitationViewSet.as_view({'delete': 'destroy'})
    update_view = TreatmentLimitationViewSet.as_view({'patch': 'partial_update'})
    model = TreatmentLimitation
    model_objects = TreatmentLimitation.objects
    model_fields = TreatmentLimitation._meta.fields


class DetectionTests(MeasureTests):
    unupdatable_fields = []
    unsettable_default_fields = dict()
    unsettable_fields = []

    objects_url = f"{PATIENTS_URL}/detection-measures"
    retrieve_view = DetectionMeasureViewSet.as_view({'get': 'retrieve'})
    list_view = DetectionMeasureViewSet.as_view({'get': 'list'})
    create_view = DetectionMeasureViewSet.as_view({'post': 'create'})
    delete_view = DetectionMeasureViewSet.as_view({'delete': 'destroy'})
    update_view = DetectionMeasureViewSet.as_view({'patch': 'partial_update'})
    model = DetectionMeasure
    model_objects = DetectionMeasure.objects
    model_fields = DetectionMeasure._meta.fields


class FailureTests(MeasureTests):
    unupdatable_fields = ["failure_type", "created_by", ]
    unsettable_default_fields = dict()
    unsettable_fields = []

    objects_url = f"{PATIENTS_URL}/failure-measures"
    retrieve_view = FailureMeasureViewSet.as_view({'get': 'retrieve'})
    list_view = FailureMeasureViewSet.as_view({'get': 'list'})
    create_view = FailureMeasureViewSet.as_view({'post': 'create'})
    delete_view = FailureMeasureViewSet.as_view({'delete': 'destroy'})
    update_view = FailureMeasureViewSet.as_view({'patch': 'partial_update'})
    model = FailureMeasure
    model_objects = FailureMeasure.objects
    model_fields = FailureMeasure._meta.fields


class PatientNoteTests(MeasureTests):
    unupdatable_fields = ["type", "created_by", "stay"]
    unsettable_default_fields = dict()
    unsettable_fields = []

    objects_url = f"{PATIENTS_URL}/patient-notes"
    retrieve_view = PatientNoteViewSet.as_view({'get': 'retrieve'})
    list_view = PatientNoteViewSet.as_view({'get': 'list'})
    create_view = PatientNoteViewSet.as_view({'post': 'create'})
    delete_view = PatientNoteViewSet.as_view({'delete': 'destroy'})
    update_view = PatientNoteViewSet.as_view({'patch': 'partial_update'})
    model = PatientNote
    model_objects = PatientNote.objects
    model_fields = PatientNote._meta.fields


# CREATE


class CreateMeasureTests(MeasureTests):
    __unittest_skip__ = True

    def test_create_measure(self):
        # As a user with access to a unit,
        # I can create a measure to a patient stay from this unit
        self.check_create_case(self.basic_create_case.clone(
            data={**self.creation_data, 'created_by': self.user_1.pk}
        ))

    def test_err_create_measure_wrong_created_by(self):
        # As a user with access to a unit,
        # I cannot create a measure specifying another user
        self.check_create_case(self.basic_create_case.clone(
            data={**self.creation_data, 'created_by': self.user_2.pk},
            success=False, status=status.HTTP_400_BAD_REQUEST
        ))

    def test_err_create_measure_wrong_data(self):
        # As a user with access to a unit,
        # I cannot create a measure with wrong data
        cases = [self.basic_create_case.clone(
            data={**self.creation_data, k: v},
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
        ) for k, v in [
            ('measure_datetime', timezone.now() + timedelta(hours=1)),
            ('stay', None)
        ]]
        [self.check_create_case(case) for case in cases]

    def test_err_create_measure_stay_not_managed(self):
        # As a user, # I cannot create a measure to a patient stay
        # from a unit I don't have access to
        case = self.basic_create_case.clone(
            data={
                **self.creation_data,
                'stay': self.stay_2.pk
            },
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
        )
        self.check_create_case(case)

    def test_err_create_measure_stay_not_current_as_admin(self):
        # Even as an admin, I cannot create a measure
        # to a patient stay that is closed
        case = self.basic_create_case.clone(
            data={
                **self.creation_data,
                'stay': self.stay_left.pk
            },
            user=self.admin_user,
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
        )
        self.check_create_case(case)


class CreateTreatmentLimitTests(TreatmentLimitationTests, CreateMeasureTests):
    __unittest_skip__ = False

    def setUp(self):
        super().setUp()

        self.test_datetime = timezone.now()
        self.creation_data = {
            'stay': self.stay_1.pk,
            'measure_datetime': self.test_datetime.isoformat(),
            'no_acr_reanimation': False,
            'no_new_failures_or_therap_raise_treatment': False,
            'no_catecholamines': False,
            'no_intubation': False,
            'no_assisted_ventilation': False,
            'no_o2': False,
            'no_eer': False,
            'no_transfusion': False,
            'no_surgery': False,
            'no_PIC_or_DVE': False,
            'no_new_antibiotherapy': False,
            'no_sirurgical_reintervetion': False,
            'no_new_complementary_exams': False,
            'no_biological_results': False,
            'pressor_amines_stop': False,
            'eer_stop': False,
            'fio2_21percent': False,
            'oxygenotherapy_stop': False,
            'mecanic_ventilation_stop': False,
            'extubation': False,
            'nutrition_stop': False,
            'hydratation_stop': False,
            'antibiotic_stop': False,
            'dve_ablation': False,
            'ecmo_stop': False,
            'all_current_therapeutics_stop': False,
            'other_stops': 'test',
            'fio2_limit': 0.5,
            'no_mecanic_ventilation_markup': False,
            'amines_limitation': 0.5,
            'no_reanimation_admittance': False,
            'treatment_limitations_comments': 'test',
        }
        self.basic_create_case = CreateCase(
            data=self.creation_data,
            retrieve_filter=MeasureCaseRetrieveFilter(
                measure_datetime=self.test_datetime
            ),
            user=self.user_1, status=status.HTTP_201_CREATED, success=True,
        )


class CreateDetectionTests(DetectionTests, CreateMeasureTests):
    __unittest_skip__ = False

    def setUp(self):
        super().setUp()
        self.test_datetime = timezone.now()

        self.creation_data = {
            'stay': self.stay_1.pk,
            'measure_datetime': self.test_datetime.isoformat(),
            'blse': False,
            'sarm': False,
            'bhc': False,
            'clostridium': False,
        }
        self.basic_create_case = CreateCase(
            data=self.creation_data,
            retrieve_filter=MeasureCaseRetrieveFilter(
                measure_datetime=self.test_datetime
            ),
            user=self.user_1, status=status.HTTP_201_CREATED, success=True,
        )


class CreateFailureTests(FailureTests, CreateMeasureTests):
    __unittest_skip__ = False

    def setUp(self):
        super().setUp()
        self.test_datetime = timezone.now()

        self.creation_data = {
            'stay': self.stay_1.pk,
            'measure_datetime': self.test_datetime.isoformat(),
            'failure_type': FailureMeasure.FailureType.lung,
            'active': True,
            'indication': 'test',
        }
        self.basic_create_case = CreateCase(
            data=self.creation_data,
            retrieve_filter=MeasureCaseRetrieveFilter(
                measure_datetime=self.test_datetime
            ),
            user=self.user_1, status=status.HTTP_201_CREATED, success=True,
        )


class CreatePatientNoteTests(PatientNoteTests, CreateMeasureTests):
    __unittest_skip__ = False

    def setUp(self):
        super().setUp()
        self.test_content = "test content"

        self.creation_data = {
            'stay': self.stay_1.pk,
            'type': PatientNote.NoteType.todo_list.value,
            'content': self.test_content,
        }
        self.basic_create_case = CreateCase(
            data=self.creation_data,
            retrieve_filter=PatientNoteCaseRetrieveFilter(
                content=self.test_content
            ),
            user=self.user_1, status=status.HTTP_201_CREATED, success=True,
        )

    def test_err_create_measure_wrong_data(self):
        # As a user with access to a unit,
        # I cannot create a patient note with wrong data
        cases = [self.basic_create_case.clone(
            data={**self.creation_data, k: v},
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
        ) for k, v in [
            ('stay', None)
        ]]
        [self.check_create_case(case) for case in cases]

    def test_err_create_duplicated_note(self):
        # Even as an admin user,
        # I cannot create another patient note of the same type on the same stay
        PatientNote.objects.create(
            stay=self.stay_1,
            type=PatientNote.NoteType.todo_list.value,
            content="existing content",
            created_by=self.user_2
        )
        self.check_create_case(self.basic_create_case.clone(
            user=self.admin_user,
            status=status.HTTP_400_BAD_REQUEST, success=False,
        ))

    def test_create_existing_general_note(self):
        # As a user with access to a unit, I can create a patient note
        # of type general even one from another user exists on the same stay
        PatientNote.objects.create(
            stay=self.stay_1,
            type=PatientNote.NoteType.general.value,
            content="existing content",
            created_by=self.user_2
        )
        self.check_create_case(self.basic_create_case.clone(
            data={**self.creation_data,
                  'type': PatientNote.NoteType.general.value},
            status=status.HTTP_201_CREATED, success=True,
        ))

    def test_err_create_existing_general_note_same_user(self):
        # As a user with access to a unit, I cannot create a patient note
        # of type general if I created one before on the same stay
        PatientNote.objects.create(
            stay=self.stay_1,
            type=PatientNote.NoteType.general.value,
            content="existing content",
            created_by=self.user_1
        )
        self.check_create_case(self.basic_create_case.clone(
            data={**self.creation_data,
                  'type': PatientNote.NoteType.general.value},
            status=status.HTTP_400_BAD_REQUEST, success=False,
        ))


# UPDATE

class UpdateMeasureTests(MeasureTests):
    __unittest_skip__ = True

    def setUp(self):
        super().setUp()
        self.other_patient_1: Patient = Patient.objects.create(
            local_id="ot_pat_1")
        self.other_stay_1: PatientStay = PatientStay.objects.create(
            patient=self.other_patient_1,
            created_by=self.user_1
        )
        self.other_bed_stay_1: BedStay = BedStay.objects.create(
            bed=self.hosp_1_beds[1],
            patient_stay=self.other_stay_1,
            start_date=timezone.now() - timedelta(days=1)
        )

    def test_update_measure(self):
        # As a user with access to a unit, I can patch a measure from
        # a stay from this unit
        self.check_patch_case(self.basic_patch_case)

    def test_err_update_measure_stay_not_managed(self):
        # As a user, I cannot patch a measure from
        # a stay from a unit I don't have access to
        case = self.basic_patch_case.clone(
            initial_data={
                **self.initial_data,
                'stay': self.stay_2
            },
            status=status.HTTP_404_NOT_FOUND,
            success=False,
        )
        self.check_patch_case(case)

    def test_err_update_measure_stay_not_current_as_admin(self):
        # Even as an admin user, I cannot patch a measure from
        # a stay that is closed
        case = self.basic_patch_case.clone(
            initial_data={
                **self.initial_data,
                'stay': self.stay_left
            },
            user=self.admin_user,
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
        )
        self.check_patch_case(case)


class UpdateTreatmentLimitTests(TreatmentLimitationTests):
    def test_err_forbidden(self):
        # Even as an admin user, I cannot patch a treatment limitation
        self.check_patch_case(PatchCase(
            initial_data={
                'stay': self.stay_1,
                'measure_datetime': timezone.now().isoformat(),
            },
            data_to_update={
                'treatment_limitations_comments': 'updated',
            },
            user=self.admin_user, status=status.HTTP_405_METHOD_NOT_ALLOWED,
            success=False,
        ))


class UpdateDetectionTests(DetectionTests):
    def test_err_forbidden(self):
        # Even as an admin user, I cannot patch a detection
        self.check_patch_case(PatchCase(
            initial_data={
                'stay': self.stay_1,
                'measure_datetime': timezone.now().isoformat(),
            },
            data_to_update={
                'blse': True,
            },
            user=self.admin_user, status=status.HTTP_405_METHOD_NOT_ALLOWED,
            success=False,
        ))


class UpdateFailureTests(UpdateMeasureTests, FailureTests):
    __unittest_skip__ = False

    def setUp(self):
        super().setUp()
        self.test_datetime = timezone.now()
        self.updated_test_datetime = self.test_datetime - timedelta(hours=1)

        self.initial_data = {
            'stay': self.stay_1,
            'measure_datetime': self.test_datetime.isoformat(),
            'failure_type': FailureMeasure.FailureType.lung,
            'active': True,
            'indication': 'test',
        }
        self.basic_patch_case = PatchCase(
            initial_data=self.initial_data,
            data_to_update={
                'stay': str(self.other_stay_1.pk),
                'measure_datetime': self.updated_test_datetime,
                'active': False,
                'indication': 'updated',
                # won't be patched
                'failure_type': FailureMeasure.FailureType.heart.value,
                'created_by': self.user_2.pk,
            },
            user=self.user_1, status=status.HTTP_200_OK, success=True,
        )


class UpdatePatientNoteTests(UpdateMeasureTests, PatientNoteTests):
    __unittest_skip__ = False

    def setUp(self):
        super().setUp()
        self.test_content = "content"
        self.updated_content = "updated"

        self.initial_data = {
            'stay': self.stay_1,
            'type': PatientNote.NoteType.todo_list.value,
            'content': self.test_content,
        }
        self.basic_patch_case = PatchCase(
            initial_data=self.initial_data,
            data_to_update={
                'content': self.updated_content,
                # won't be patched
                'stay': self.other_stay_1.pk,
                'created_by': self.user_2.pk,
                'type': PatientNote.NoteType.general.value,
            },
            user=self.user_1, status=status.HTTP_200_OK, success=True,
        )


# DELETE


class DeleteMeasureTests(MeasureTests):
    __unittest_skip__ = True

    def test_delete_measure(self):
        # As a user with access to a unit, I can delete a patient note from
        # a stay from this unit
        self.check_delete_case(self.basic_delete_case)

    def test_err_delete_measure_stay_not_managed(self):
        # As a user, I cannot patch a patient note from
        # a stay from a unit I don't have access to
        case = self.basic_delete_case.clone(
            data_to_delete={
                **self.initial_data,
                'stay': self.stay_2
            },
            status=status.HTTP_404_NOT_FOUND,
            success=False,
        )
        self.check_delete_case(case)

    def test_err_delete_measure_stay_not_current_as_admin(self):
        # Even as an admin user, I cannot delete a patient note from
        # a stay that is closed
        case = self.basic_delete_case.clone(
            data_to_delete={
                **self.initial_data,
                'stay': self.stay_left
            },
            user=self.admin_user,
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
        )
        self.check_delete_case(case)


class DeleteTreatmentLimitTests(DeleteMeasureTests, TreatmentLimitationTests):
    __unittest_skip__ = False

    def setUp(self):
        super().setUp()
        # DeleteMeasureTests.setUp(self)

        self.initial_data = {
            'stay': self.stay_1,
            'measure_datetime': timezone.now(),
        }
        self.basic_delete_case = DeleteCase(
            data_to_delete=self.initial_data,
            user=self.user_1, status=status.HTTP_204_NO_CONTENT, success=True,
        )


class DeleteDetectionTests(DeleteMeasureTests, DetectionTests):
    __unittest_skip__ = False

    def setUp(self):
        super().setUp()
        # DeleteMeasureTests.setUp(self)

        self.initial_data = {
            'stay': self.stay_1,
            'measure_datetime': timezone.now(),
        }
        self.basic_delete_case = DeleteCase(
            data_to_delete=self.initial_data,
            user=self.user_1, status=status.HTTP_204_NO_CONTENT, success=True,
        )


class DeleteFailureTests(DeleteMeasureTests, FailureTests):
    __unittest_skip__ = False

    def setUp(self):
        super().setUp()
        # DeleteMeasureTests.setUp(self)

        self.initial_data = {
            'stay': self.stay_1,
            'measure_datetime': timezone.now(),
            'active': True,
        }
        self.basic_delete_case = DeleteCase(
            data_to_delete=self.initial_data,
            user=self.user_1, status=status.HTTP_204_NO_CONTENT, success=True,
        )


class DeletePatientNoteTests(DeleteMeasureTests, PatientNoteTests):
    __unittest_skip__ = False

    def setUp(self):
        super().setUp()
        self.test_content = "content"

        self.initial_data = {
            'stay': self.stay_1,
            'type': PatientNote.NoteType.todo_list.value,
            'content': self.test_content,
        }
        self.basic_delete_case = DeleteCase(
            data_to_delete=self.initial_data,
            user=self.user_1, status=status.HTTP_204_NO_CONTENT, success=True,
        )


# GET

class MeasureGetListTests(ViewSetTestsWithBasicInfra):
    __unittest_skip__ = True

    def random_str_value(self, perfect_match_possible: bool = False):
        return random_str(
            randint(5, 7),
            include_pattern=(self.name_pattern
                             if random.random() > 0.8 else random_str(3))
        ) if (not perfect_match_possible or random.random()) > 0.2 \
            else self.name_pattern

    def setUp(self):
        ViewSetTestsWithBasicInfra.setUp(self)
        self.admin_user: User = new_random_user("admin", "user", is_staff=True)
        self.user_1: User = new_random_user("user", "1")
        self.user_2: User = new_random_user("user", "2")
        self.access_user_1_serv_1: Access = Access.objects.create(
            intensive_care_service=self.serv_1,
            user=self.user_1,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=False,
            right_review=True
        )
        self.access_user_2_serv_2: Access = Access.objects.create(
            intensive_care_service=self.serv_2,
            user=self.user_2,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=False,
            right_review=True
        )

        self.name_pattern = random_str(3).replace(' ', 'e')
        self.test_date = date.fromisoformat("2000-01-01")
        self.test_float = 0.5
        nb_patients = 500

        self.list_patients: List[Patient] = Patient.objects.bulk_create([
            Patient(local_id=self.random_str_value()) for _ in range(nb_patients)
        ])
        self.list_patient_stays: List[PatientStay] = (
            PatientStay.objects.bulk_create([
                PatientStay(
                    created_by=self.user_1,
                    patient=p
                ) for p in self.list_patients
            ])
        )
        BedStay.objects.bulk_create(
            [BedStay(
                start_date=timezone.now() - timedelta(days=2),
                end_date=(None if random.random() > 0.8
                          else timezone.now() - timedelta(days=1)),
                patient_stay=ps,
                bed=random.choice(self.hosp_1_beds + self.hosp_2_beds)
            ) for ps in self.list_patient_stays]
        )

        self.basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                                    user=self.admin_user)

    def test_get_all_measures_as_admin(self):
        # As a user with is_staff=True, I can get all measures
        case = ListCase(
            to_find=self.measures,
            success=True,
            status=status.HTTP_200_OK,
            user=self.admin_user
        )
        self.check_get_paged_list_case(case)

    def test_get_all_measures_from_my_service(self):
        # As a user with access to a service, I can get all the measures from
        # this service
        case = ListCase(
            to_find=[m for m in self.measures
                     if m.stay.last_bed in self.hosp_1_beds],
            success=True,
            status=status.HTTP_200_OK,
            user=self.user_1
        )
        self.check_get_paged_list_case(case)

    def test_error_not_authenticated(self):
        # As a user not authenticated, I cannot get any measure
        case = ListCase(
            user=None,
            to_find=[],
            success=False,
            status=status.HTTP_401_UNAUTHORIZED
        )
        self.check_get_paged_list_case(case)

    def test_get_list_with_stay_param(self):
        # As a user, I can get the measures filtered with stay filter
        stay = self.list_patient_stays[0]
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"stay={stay}",
            to_find=[
                p for p in self.measures
                if p.stay.pk == stay.pk
            ],
            params=dict(stay=stay.pk)
        ))

    def test_get_list_with_created_by_param(self):
        # As a user, I can get measures filtered with created_by filter
        user = self.user_1
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"created_by={user}",
            to_find=[
                p for p in self.measures
                if p.created_by.pk == user.pk
            ],
            params=dict(created_by=user.pk)
        ))


class TreatmentLimitGetListTests(MeasureGetListTests, TreatmentLimitationTests):
    __unittest_skip__ = False

    def setUp(self):
        super(TreatmentLimitGetListTests, self).setUp()

        def random_float():
            return self.test_float + random.choice(
                [-0.1, 0, 0.1]
            )

        self.measures: List[TreatmentLimitation] = (
            TreatmentLimitation.objects.bulk_create([
                TreatmentLimitation(
                    stay=s,
                    created_by=random.choice([self.user_1, self.user_2]),
                    measure_datetime=timezone.now(),
                    no_acr_reanimation=random.random() > 0.5,
                    no_new_failures_or_therap_raise_treatment=(random.random()
                                                               > 0.5),
                    no_catecholamines=random.random() > 0.5,
                    no_intubation=random.random() > 0.5,
                    no_assisted_ventilation=random.random() > 0.5,
                    no_o2=random.random() > 0.5,
                    no_eer=random.random() > 0.5,
                    no_transfusion=random.random() > 0.5,
                    no_surgery=random.random() > 0.5,
                    no_PIC_or_DVE=random.random() > 0.5,
                    no_new_antibiotherapy=random.random() > 0.5,
                    no_sirurgical_reintervetion=random.random() > 0.5,
                    no_new_complementary_exams=random.random() > 0.5,
                    no_biological_results=random.random() > 0.5,
                    pressor_amines_stop=random.random() > 0.5,
                    eer_stop=random.random() > 0.5,
                    fio2_21percent=random.random() > 0.5,
                    oxygenotherapy_stop=random.random() > 0.5,
                    mecanic_ventilation_stop=random.random() > 0.5,
                    extubation=random.random() > 0.5,
                    nutrition_stop=random.random() > 0.5,
                    hydratation_stop=random.random() > 0.5,
                    antibiotic_stop=random.random() > 0.5,
                    dve_ablation=random.random() > 0.5,
                    ecmo_stop=random.random() > 0.5,
                    all_current_therapeutics_stop=random.random() > 0.5,
                    other_stops=self.random_str_value(True),
                    fio2_limit=random_float(),
                    no_mecanic_ventilation_markup=random.random() > 0.5,
                    amines_limitation=random_float(),
                    no_reanimation_admittance=random.random() > 0.5,
                    treatment_limitations_comments=self.random_str_value(True),
                ) for s in self.list_patient_stays
            ])
        )

    def test_get_list_with_search_param(self):
        # As an admin user, I can get patients filtered with search filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"search={self.name_pattern.lower()}",
            to_find=[
                tl for tl in self.measures
                if any(self.name_pattern.lower() in getattr(tl, f).lower()
                       for f in ["other_stops",
                                 "treatment_limitations_comments"])
            ],
            params=dict(search=self.name_pattern)
        ))

    def test_get_list_with_no_acr_reanimation_param(self):
        # As an admin user, I can get tl filtered with no_acr_reanimation filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_acr_reanimation=False",
            to_find=[
                p for p in self.measures
                if not p.no_acr_reanimation
            ],
            params=dict(no_acr_reanimation=False)
        ))

    def test_get_list_with_no_new_failures_or_therap_raise_treat_param(self):
        # As an admin user, I can get tl filtered with
        # no_new_failures_or_therap_raise_treatment filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_new_failures_or_therap_raise_treatment=False",
            to_find=[
                p for p in self.measures
                if not p.no_new_failures_or_therap_raise_treatment
            ],
            params=dict(no_new_failures_or_therap_raise_treatment=False)
        ))

    def test_get_list_with_no_catecholamines_param(self):
        # As an admin user, I can get tl filtered with no_catecholamines filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_catecholamines=False",
            to_find=[
                p for p in self.measures
                if not p.no_catecholamines
            ],
            params=dict(no_catecholamines=False)
        ))

    def test_get_list_with_no_intubation_param(self):
        # As an admin user, I can get tl filtered with no_intubation filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_intubation=False",
            to_find=[
                p for p in self.measures
                if not p.no_intubation
            ],
            params=dict(no_intubation=False)
        ))

    def test_get_list_with_no_assisted_ventilation_param(self):
        # As an admin user, I can get tl filtered
        # with no_assisted_ventilation filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_assisted_ventilation=False",
            to_find=[
                p for p in self.measures
                if not p.no_assisted_ventilation
            ],
            params=dict(no_assisted_ventilation=False)
        ))

    def test_get_list_with_no_o2_param(self):
        # As an admin user, I can get tl filtered with no_o2 filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_o2=False",
            to_find=[
                p for p in self.measures
                if not p.no_o2
            ],
            params=dict(no_o2=False)
        ))

    def test_get_list_with_no_eer_param(self):
        # As an admin user, I can get tl filtered with no_eer filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_eer=False",
            to_find=[
                p for p in self.measures
                if not p.no_eer
            ],
            params=dict(no_eer=False)
        ))

    def test_get_list_with_no_transfusion_param(self):
        # As an admin user, I can get tl filtered with no_transfusion filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_transfusion=False",
            to_find=[
                p for p in self.measures
                if not p.no_transfusion
            ],
            params=dict(no_transfusion=False)
        ))

    def test_get_list_with_no_surgery_param(self):
        # As an admin user, I can get tl filtered with no_surgery filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_surgery=False",
            to_find=[
                p for p in self.measures
                if not p.no_surgery
            ],
            params=dict(no_surgery=False)
        ))

    def test_get_list_with_no_PIC_or_DVE_param(self):
        # As an admin user, I can get tl filtered with no_PIC_or_DVE filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_PIC_or_DVE=False",
            to_find=[
                p for p in self.measures
                if not p.no_PIC_or_DVE
            ],
            params=dict(no_PIC_or_DVE=False)
        ))

    def test_get_list_with_no_new_antibiotherapy_param(self):
        # As an admin user, I can get tl filtered with
        # no_new_antibiotherapy filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_new_antibiotherapy=False",
            to_find=[
                p for p in self.measures
                if not p.no_new_antibiotherapy
            ],
            params=dict(no_new_antibiotherapy=False)
        ))

    def test_get_list_with_no_sirurgical_reintervetion_param(self):
        # As an admin user, I can get tl filtered with
        # no_sirurgical_reintervetion filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_sirurgical_reintervetion=False",
            to_find=[
                p for p in self.measures
                if not p.no_sirurgical_reintervetion
            ],
            params=dict(no_sirurgical_reintervetion=False)
        ))

    def test_get_list_with_no_new_complementary_exams_param(self):
        # As an admin user, I can get tl filtered with
        # no_new_complementary_exams filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_new_complementary_exams=False",
            to_find=[
                p for p in self.measures
                if not p.no_new_complementary_exams
            ],
            params=dict(no_new_complementary_exams=False)
        ))

    def test_get_list_with_no_biological_results_param(self):
        # As an admin user, I can get tl filtered with
        # no_biological_results filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_biological_results=False",
            to_find=[
                p for p in self.measures
                if not p.no_biological_results
            ],
            params=dict(no_biological_results=False)
        ))

    def test_get_list_with_pressor_amines_stop_param(self):
        # As an admin user, I can get tl filtered with
        # pressor_amines_stop filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="pressor_amines_stop=False",
            to_find=[
                p for p in self.measures
                if not p.pressor_amines_stop
            ],
            params=dict(pressor_amines_stop=False)
        ))

    def test_get_list_with_eer_stop_param(self):
        # As an admin user, I can get tl filtered with eer_stop filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="eer_stop=False",
            to_find=[
                p for p in self.measures
                if not p.eer_stop
            ],
            params=dict(eer_stop=False)
        ))

    def test_get_list_with_fio2_21percent_param(self):
        # As an admin user, I can get tl filtered with fio2_21percent filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="fio2_21percent=False",
            to_find=[
                p for p in self.measures
                if not p.fio2_21percent
            ],
            params=dict(fio2_21percent=False)
        ))

    def test_get_list_with_oxygenotherapy_stop_param(self):
        # As an admin user, I can get tl filtered with
        # oxygenotherapy_stop filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="oxygenotherapy_stop=False",
            to_find=[
                p for p in self.measures
                if not p.oxygenotherapy_stop
            ],
            params=dict(oxygenotherapy_stop=False)
        ))

    def test_get_list_with_mecanic_ventilation_stop_param(self):
        # As an admin user, I can get tl filtered with
        # mecanic_ventilation_stop filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="mecanic_ventilation_stop=False",
            to_find=[
                p for p in self.measures
                if not p.mecanic_ventilation_stop
            ],
            params=dict(mecanic_ventilation_stop=False)
        ))

    def test_get_list_with_extubation_param(self):
        # As an admin user, I can get tl filtered with extubation filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="extubation=False",
            to_find=[
                p for p in self.measures
                if not p.extubation
            ],
            params=dict(extubation=False)
        ))

    def test_get_list_with_nutrition_stop_param(self):
        # As an admin user, I can get tl filtered with nutrition_stop filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="nutrition_stop=False",
            to_find=[
                p for p in self.measures
                if not p.nutrition_stop
            ],
            params=dict(nutrition_stop=False)
        ))

    def test_get_list_with_hydratation_stop_param(self):
        # As an admin user, I can get tl filtered with hydratation_stop filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="hydratation_stop=False",
            to_find=[
                p for p in self.measures
                if not p.hydratation_stop
            ],
            params=dict(hydratation_stop=False)
        ))

    def test_get_list_with_antibiotic_stop_param(self):
        # As an admin user, I can get tl filtered with antibiotic_stop filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="antibiotic_stop=False",
            to_find=[
                p for p in self.measures
                if not p.antibiotic_stop
            ],
            params=dict(antibiotic_stop=False)
        ))

    def test_get_list_with_dve_ablation_param(self):
        # As an admin user, I can get tl filtered with dve_ablation filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="dve_ablation=False",
            to_find=[
                p for p in self.measures
                if not p.dve_ablation
            ],
            params=dict(dve_ablation=False)
        ))

    def test_get_list_with_ecmo_stop_param(self):
        # As an admin user, I can get tl filtered with ecmo_stop filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="ecmo_stop=False",
            to_find=[
                p for p in self.measures
                if not p.ecmo_stop
            ],
            params=dict(ecmo_stop=False)
        ))

    def test_get_list_with_all_current_therapeutics_stop_param(self):
        # As an admin user, I can get tl filtered
        # with all_current_therapeutics_stop filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="all_current_therapeutics_stop=False",
            to_find=[
                p for p in self.measures
                if not p.all_current_therapeutics_stop
            ],
            params=dict(all_current_therapeutics_stop=False)
        ))

    def test_get_list_with_other_stops_param(self):
        # As an admin user, I can get tl filtered with other_stops filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"other_stops={self.name_pattern}",
            to_find=[
                p for p in self.measures
                if self.name_pattern == p.other_stops
            ],
            params=dict(other_stops=self.name_pattern)
        ))

    def test_get_list_with_fio2_limit_param(self):
        # As an admin user, I can get tl filtered with fio2_limit filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"fio2_limit={self.test_float}",
            to_find=[
                p for p in self.measures
                if p.fio2_limit == self.test_float
            ],
            params=dict(fio2_limit=self.test_float)
        ))

    def test_get_list_with_no_mecanic_ventilation_markup_param(self):
        # As an admin user, I can get tl filtered
        # with no_mecanic_ventilation_markup filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_mecanic_ventilation_markup=False",
            to_find=[
                p for p in self.measures
                if not p.no_mecanic_ventilation_markup
            ],
            params=dict(no_mecanic_ventilation_markup=False)
        ))

    def test_get_list_with_amines_limitation_param(self):
        # As an admin user, I can get tl filtered with amines_limitation filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"amines_limitation={self.test_float}",
            to_find=[
                p for p in self.measures
                if p.amines_limitation == self.test_float
            ],
            params=dict(amines_limitation=self.test_float)
        ))

    def test_get_list_with_no_reanimation_admittance_param(self):
        # As an admin user, I can get tl filtered
        # with no_reanimation_admittance filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="no_reanimation_admittance=False",
            to_find=[
                p for p in self.measures
                if not p.no_reanimation_admittance
            ],
            params=dict(no_reanimation_admittance=False)
        ))

    def test_get_list_with_treatment_limitations_commentsZ_param(self):
        # As an admin user, I can get tl filtered
        # with treatment_limitations_comments filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"treatment_limitations_comments={self.name_pattern}",
            to_find=[
                p for p in self.measures
                if self.name_pattern == p.treatment_limitations_comments
            ],
            params=dict(treatment_limitations_comments=self.name_pattern)
        ))


class DetectionGetListTests(MeasureGetListTests, DetectionTests):
    __unittest_skip__ = False

    def setUp(self):
        super(DetectionGetListTests, self).setUp()

        self.measures: List[DetectionMeasure] = (
            DetectionMeasure.objects.bulk_create([
                DetectionMeasure(
                    stay=s,
                    created_by=random.choice([self.user_1, self.user_2]),
                    measure_datetime=timezone.now(),
                    blse=random.random() > 0.5,
                    sarm=random.random() > 0.5,
                    bhc=random.random() > 0.5,
                    clostridium=random.random() > 0.5,
                ) for s in self.list_patient_stays
            ])
        )

    def test_get_list_with_blse_param(self):
        # As an admin user, I can get detections filtered with blse filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="blse=False",
            to_find=[
                m for m in self.measures
                if not m.blse
            ],
            params=dict(blse=False)
        ))

    def test_get_list_with_sarm_param(self):
        # As an admin user, I can get detections filtered with sarm filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="sarm=False",
            to_find=[
                m for m in self.measures
                if not m.sarm
            ],
            params=dict(sarm=False)
        ))

    def test_get_list_with_bhc_param(self):
        # As an admin user, I can get detections filtered with bhc filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="bhc=False",
            to_find=[
                m for m in self.measures
                if not m.bhc
            ],
            params=dict(bhc=False)
        ))

    def test_get_list_with_clostridium_param(self):
        # As an admin user, I can get detections filtered
        # with clostridium filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="clostridium=False",
            to_find=[
                m for m in self.measures
                if not m.clostridium
            ],
            params=dict(clostridium=False)
        ))


class FailureGetListTests(MeasureGetListTests, FailureTests):
    __unittest_skip__ = False

    def setUp(self):
        super(FailureGetListTests, self).setUp()

        self.measures: List[FailureMeasure] = (
            FailureMeasure.objects.bulk_create([
                FailureMeasure(
                    stay=s,
                    created_by=random.choice([self.user_1, self.user_2]),
                    measure_datetime=timezone.now(),
                    failure_type=random.choice(
                        FailureMeasure.FailureType.values
                    ),
                    active=random.random() > 0.5,
                    indication=self.random_str_value(True),
                ) for s in self.list_patient_stays
            ])
        )

    def test_get_list_with_failure_type_param(self):
        # As an admin user, I can get failures filtered with failure_type filter
        test_type = FailureMeasure.FailureType.lung.value
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"failure_type={test_type}",
            to_find=[
                m for m in self.measures
                if m.failure_type == test_type
            ],
            params=dict(failure_type=test_type)
        ))

    def test_get_list_with_active_param(self):
        # As an admin user, I can get failures filtered with active filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title="active=False",
            to_find=[
                m for m in self.measures
                if not m.active
            ],
            params=dict(active=False)
        ))

    def test_get_list_with_indication_param(self):
        # As an admin user, I can get failures filtered with indication filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"indication={self.name_pattern}",
            to_find=[
                m for m in self.measures
                if m.indication == self.name_pattern
            ],
            params=dict(indication=self.name_pattern)
        ))


class PatientNoteGetListTests(MeasureGetListTests, PatientNoteTests):
    __unittest_skip__ = False

    def setUp(self):
        super(PatientNoteGetListTests, self).setUp()

        self.measures: List[PatientNote] = (
            PatientNote.objects.bulk_create([
                PatientNote(
                    stay=s,
                    created_by=random.choice([self.user_1, self.user_2]),
                    type=random.choice(
                        PatientNote.NoteType.values
                    ),
                    content=self.random_str_value(True),
                ) for s in self.list_patient_stays
            ])
        )

    def test_get_list_with_type_param(self):
        # As an admin user, I can get patient notes filtered with type filter
        test_type = PatientNote.NoteType.general.value
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"type={test_type}",
            to_find=[
                m for m in self.measures
                if m.type == test_type
            ],
            params=dict(type=test_type)
        ))

    def test_get_list_with_content_param(self):
        # As an admin user, I can get patient notes filtered with content filter
        self.check_get_paged_list_case(ListCase(
            **self.basic_case_dict,
            title=f"content={self.name_pattern}",
            to_find=[
                m for m in self.measures
                if m.content == self.name_pattern
            ],
            params=dict(content=self.name_pattern)
        ))


# other

#
#
# class PatchMeasureTests(MeasureTests):
#     def check_patch_case(self, case: PatientPatchCase):
#         obj = self.init_case_obj(case)
#         super(PatchMeasureTests, self).check_patch_case(case, obj)
#
#     def setUp(self):
#         super().setUp()
#         test_local_id = "test_id"
#
#         self.creation_data = {
#             'local_id': test_local_id,
#             'first_name': 'test',
#             'last_name': 'test',
#             'birth_date': date.fromisoformat("2000-01-01"),
#             'sex': Patient.PatientSex.undefined,
#             'size_cm': 150,
#             'weight_kg': 50,
#             'antecedents': "{}",
#             'allergies': "{}",
#         }
#
#         self.basic_patch_case = PatientPatchCase(
#             bed=self.hosp_1_beds[0],
#             initial_data=self.creation_data,
#             data_to_update=dict(
#                 first_name='test',
#                 last_name='test',
#                 birth_date=date.fromisoformat("2000-01-01"),
#                 sex=Patient.PatientSex.undefined,
#                 size_cm=150,
#                 weight_kg=50,
#                 antecedents="{}",
#                 allergies="{}",
#             ),
#             user=self.user_1,
#             success=True,
#             status=status.HTTP_200_OK,
#         )
#
#     def test_patch_patient(self):
#         self.check_patch_case(self.basic_patch_case)
#
#     def test_err_patch_patient_other_service(self):
#         case = self.basic_patch_case.clone(
#             bed=self.hosp_2_beds[0],
#             success=False,
#             status=status.HTTP_404_NOT_FOUND
#         )
#
#
# class DeleteMeasureTests(MeasureTests):
#     def check_delete_case(self, case: PatientDeleteCase):
#         obj = self.init_case_obj(case) if case.bed else None
#         super(DeleteMeasureTests, self).check_delete_case(case, obj)
#
#     def setUp(self):
#         super().setUp()
#         test_local_id = "test_id"
#         self.admin_user: User = new_random_user("admin", "main", is_staff=True)
#
#         self.basic_delete_case = PatientDeleteCase(
#             bed=None,
#             data_to_delete={
#             'local_id': test_local_id,
#             'first_name': 'test',
#             'last_name': 'test',
#             'birth_date': date.fromisoformat("2000-01-01"),
#             'sex': Patient.PatientSex.undefined,
#             'size_cm': 150,
#             'weight_kg': 50,
#             'antecedents': "{}",
#             'allergies': "{}",
#         },
#             user=self.admin_user,
#             success=True,
#             status=status.HTTP_204_NO_CONTENT,
#         )
#
#     def test_delete_patient_with_no_stay_as_admin(self):
#         self.check_delete_case(self.basic_delete_case)
#
#     def test_err_delete_patient_as_not_admin(self):
#         self.check_delete_case(self.basic_delete_case.clone(
#             user=self.user_1,
#             success=False,
#             status=status.HTTP_404_NOT_FOUND,
#         ))
#
#     def test_err_delete_patient_with_current_stay(self):
#         self.check_delete_case(self.basic_delete_case.clone(
#             bed=self.hosp_1_beds[0],
#             success=False,
#             status=status.HTTP_400_BAD_REQUEST,
#         ))
#
#     def test_err_delete_patient_with_current_stay_not_as_admin(self):
#         self.check_delete_case(self.basic_delete_case.clone(
#             user=self.user_1,
#             bed=self.hosp_1_beds[0],
#             success=False,
#             status=status.HTTP_403_FORBIDDEN,
#         ))
