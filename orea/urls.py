from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework.routers import DefaultRouter

from drf_spectacular.views import SpectacularSwaggerView, SpectacularAPIView

from orea.views import UserViewSet, BackendStateView


class OptionalSlashRouter(DefaultRouter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.trailing_slash = '/?'


router = OptionalSlashRouter()
router.register(r'users', UserViewSet, basename="users")


urlpatterns = [
    path(r'', include(router.urls)),
    path('admin', admin.site.urls),
    path("accounts/", include("orea.urls_login")),
    path("infra/", include('infra.urls')),
    path("patients/", include('patients.urls')),
    re_path("state", BackendStateView.as_view(), name="backend_state"),
    path("users-app/", include('users.urls')),
    path("voting/", include('voting.urls')),
    path("schema", SpectacularAPIView.as_view(), name="schema"),
    path("docs", SpectacularSwaggerView.as_view(url_name='schema'),
         name='swagger-ui'),
]
