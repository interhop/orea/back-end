from django.views import View
from rest_framework import permissions
from rest_framework.permissions import SAFE_METHODS

from rest_framework.request import Request

from orea.models import BaseModel


class IsSelf(permissions.IsAuthenticated):
    def has_permission(self, request: Request, view: View):
        return request.method in ("GET", "PATCH",)

    def has_object_permission(self, request: Request,
                              view: View, obj: BaseModel):
        return request.user.pk == obj.pk and request.method in ("GET", "PATCH",)


class IsAdminOrReadOnly(permissions.IsAdminUser):
    def has_permission(self, request, view):
        return ((request.user and request.user.is_authenticated
                 and request.method in SAFE_METHODS)
                or super(IsAdminOrReadOnly, self).has_permission(request, view))
