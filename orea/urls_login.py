from django.urls import re_path, include

from dj_rest_auth.views import PasswordResetView, PasswordResetConfirmView, \
    PasswordChangeView

from .settings import ALLOW_ACCOUNT_MANAGEMENT
from .urls import OptionalSlashRouter
from .views import CustomLoginView, get_custom_refresh_view, CustomLogoutView

# CustomSigninView


router = OptionalSlashRouter()

app_name = ''
urlpatterns = [
    re_path(r'^login', CustomLoginView.as_view(), name='login'),
    re_path(r'refresh', get_custom_refresh_view().as_view(),
            name='token_refresh'),
    re_path(r'^logout', CustomLogoutView.as_view(), name='logout'),

    *([
        # from rest-auth
        re_path(r'^password/reset/$', PasswordResetView.as_view(),
                name='rest_password_reset'),
        re_path(r'^password/reset/confirm/$',
                PasswordResetConfirmView.as_view(),
                name='rest_password_reset_confirm'),
        re_path(r'^password/change/$', PasswordChangeView.as_view(),
                name='rest_password_change'),
        re_path(r'registration/', include('dj_rest_auth.registration.urls'))
    ] if ALLOW_ACCOUNT_MANAGEMENT else []),
]
