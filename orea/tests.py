from __future__ import annotations

import random
from datetime import timedelta
from typing import Optional, List
from unittest.mock import patch, MagicMock

from dj_rest_auth.app_settings import JWT_AUTH_COOKIE, JWT_AUTH_REFRESH_COOKIE
from dj_rest_auth.utils import jwt_encode
from django.db.models import Model
from django.utils import timezone
from requests import Response
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient, ForceAuthClientHandler, \
    force_authenticate
from rest_framework_simplejwt.serializers import TokenRefreshSerializer
from rest_framework_simplejwt.settings import api_settings
from rest_framework_simplejwt.tokens import AccessToken
from uuid import uuid4

from infra.models import IntensiveCareService
from infra.tests.tools import ViewSetTestsWithBasicInfra
from users.models import Access
from .models import User
from .tests_tools import new_random_user, BaseTests, RequestCase, PatchCase,\
    random_str, ListCase
from .types import JwtTokens, ServerError, LoginError
from .views import UserViewSet


class BackendStateTests(BaseTests):
    @patch('orea.views.DEMO_USER_USERNAME', "test")
    @patch('orea.views.ALLOW_ACCOUNT_MANAGEMENT', False)
    @patch('orea.views.__version__', '1.0.0')
    def test_get_state(self):
        resp = self.client.get(reverse("backend_state"))
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.json().get('version', None), "1.0.0")
        self.assertEqual(resp.json().get('canManageAccounts', None), False)
        self.assertEqual(resp.json().get('demoUsername', ""), "test")

    @patch('orea.views.DEMO_USER_USERNAME', "")
    def test_get_state_username_null(self):
        resp = self.client.get(reverse("backend_state"))
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIsNone(resp.json().get('demoUsername', None))


class LoginCase(RequestCase):
    def __init__(self, username: str, password: str,
                 check_ids_side_effect: Optional[Exception],
                 user_expected: Optional[User], **kwargs):
        super(LoginCase, self).__init__(**kwargs)
        self.username = username
        self.password = password
        self.user_expected = user_expected
        self.check_ids_side_effect = check_ids_side_effect

    def clone(self, **kwargs) -> LoginCase:
        return self.__class__(**{**self.__dict__, **kwargs})


class LoginTests(BaseTests):
    def setUp(self):
        self.password = "password"
        self.user = new_random_user(username="username",
                                    password=self.password)
        self.base_login_case = LoginCase(
            status=status.HTTP_200_OK,
            success=True,
            user=None,
            username=self.user.username,
            password=self.password,
            user_expected=self.user,
            check_ids_side_effect=None
        )
        self.base_login_err_case = self.base_login_case.clone(
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
            user_expected=None,
        )

    def check_login(self, case: LoginCase, mock_conf_auth: MagicMock,
                    mock_jwt: MagicMock = None):
        test_access = 'access'
        test_refresh = 'refresh'

        if case.check_ids_side_effect:
            mock_conf_auth.check_ids.side_effect = case.check_ids_side_effect
            if mock_jwt:
                mock_jwt.return_value = (test_access, test_refresh)
        else:
            mock_conf_auth.check_ids.return_value = JwtTokens(
                access=test_access,
                refresh=test_refresh,
                last_connection=dict(created_at='test')
            )

        resp: Response = self.client.post(reverse('login'), data=dict(
            username=case.username, password=case.password
        ), format="json")
        self.assertEqual(resp.status_code, case.status)

        if case.success:
            self.assertEqual(resp.json().get('accessToken', None), test_access)
            self.assertEqual(resp.json().get('refreshToken', None), test_refresh)

            resp_user = resp.json().get('user', {})
            self.assertEqual(resp_user.get('uuid', None),
                             str(case.user_expected.pk))
            self.assertEqual(resp_user.get('username', None),
                             case.user_expected.username)
            self.assertEqual(resp_user.get('email', None),
                             case.user_expected.email)
            self.assertEqual(resp_user.get('firstName', None),
                             case.user_expected.first_name)
            self.assertEqual(resp_user.get('lastName', None),
                             case.user_expected.last_name)

            self.assertIn('access', resp.cookies)
            self.assertIn('refresh', resp.cookies)
            self.assertIn(resp.cookies['access'].value, test_access)
            self.assertIn(resp.cookies['refresh'].value, test_refresh)

        if case.check_ids_side_effect:
            mock_conf_auth.get_user.assert_not_called()
            if (isinstance(case.check_ids_side_effect, NotImplementedError)
                    and mock_jwt
                    and case.user_expected):
                mock_jwt.assert_called_once_with(case.user_expected)
        else:
            mock_conf_auth.get_user.assert_called_once_with(test_access)

    @patch('orea.authentication.conf_auth')
    def test_login(self, mock_conf_auth: MagicMock):
        # As a non-authenticated user, I can log in and conf_auth will be called
        mock_conf_auth.get_user.return_value = self.user
        self.check_login(self.base_login_case, mock_conf_auth)

    @patch('orea.authentication.conf_auth')
    def test_err_login_check_ids_login_err(self, mock_conf_auth: MagicMock):
        # As a non-authenticated user, I cannot log in
        # if check_ids is implemented and it returns error
        mock_conf_auth.get_user.return_value = self.user
        self.check_login(self.base_login_err_case.clone(
            check_ids_side_effect=LoginError()
        ), mock_conf_auth)

    @patch('orea.authentication.conf_auth')
    def test_err_login_check_ids_server_err(self, mock_conf_auth: MagicMock):
        # As a non-authenticated user, I cannot log in
        # if check_ids is implemented and it returns error
        mock_conf_auth.get_user.return_value = self.user
        self.check_login(self.base_login_err_case.clone(
            check_ids_side_effect=ServerError(),
            status=status.HTTP_503_SERVICE_UNAVAILABLE
        ), mock_conf_auth)

    @patch('orea.authentication.conf_auth')
    def test_login_get_user_not_implemented(self, mock_conf_auth: MagicMock):
        # As a non-authenticated user, I can log in with default
        # 'get_user' if not implemented in conf_auth
        mock_conf_auth.get_user.side_effect = NotImplementedError
        self.check_login(self.base_login_case, mock_conf_auth)

    @patch('orea.authentication.conf_auth')
    def test_err_login_get_user_not_implemented_user_not_existing(
            self, mock_conf_auth: MagicMock
    ):
        # As a non-authenticated user,
        # I cannot log in with wrong credentials and
        # default 'get_user' when not implemented in conf_auth
        mock_conf_auth.get_user.side_effect = NotImplementedError
        self.check_login(self.base_login_err_case.clone(
            username='random',
        ), mock_conf_auth)

    @patch('orea.authentication.conf_auth')
    @patch('orea.authentication.jwt_encode')
    def test_login_check_ids_not_implemented(
            self, mock_jwt: MagicMock, mock_conf_auth: MagicMock
    ):
        # As a non-authenticated user, I can log in with default
        # 'check_ids' if not implemented in conf_auth
        mock_conf_auth.get_user.return_value = self.user
        self.check_login(self.base_login_case.clone(
            check_ids_side_effect=NotImplementedError()
        ), mock_conf_auth, mock_jwt)

    @patch('orea.authentication.conf_auth')
    @patch('orea.authentication.jwt_encode')
    def test_err_login_check_ids_not_implemented_wrong_username(
            self, mock_jwt: MagicMock,
            mock_conf_auth: MagicMock
    ):
        # As a non-authenticated user, I cannot log in with worng crendentials
        # with default 'check_ids' when not implemented in conf_auth
        mock_conf_auth.get_user.return_value = self.user
        self.check_login(self.base_login_err_case.clone(
            username='random',
            check_ids_side_effect=NotImplementedError()
        ), mock_conf_auth, mock_jwt)

    @patch('orea.authentication.conf_auth')
    def test_err_login_ids_checked_but_user_not_found(
            self, mock_conf_auth: MagicMock
    ):
        # As a non-authenticated user, I can log in and conf_auth will be called
        mock_conf_auth.get_user.return_value = None
        self.check_login(self.base_login_err_case, mock_conf_auth)

    @patch('orea.authentication.conf_auth')
    def test_err_login_ids_checked_but_user_not_found_2(
            self, mock_conf_auth: MagicMock
    ):
        # As a non-authenticated user, I can log in and conf_auth will be called
        mock_conf_auth.get_user.side_effect = NotImplementedError()
        self.check_login(self.base_login_err_case.clone(
            username="random",
        ), mock_conf_auth)


class CustomClientHandler(ForceAuthClientHandler):
    def get_response(self, request):
        request.COOKIES.update(request.META.get('COOKIES', {}))
        return super().get_response(request)


class CustomAPIClient(APIClient):
    """
    Changes handler with one that will add cookies if 'COOKIES' is set in
    get function
    """
    def __init__(
        self, enforce_csrf_checks=False, raise_request_exception=True,
        **defaults
    ):
        super().__init__(enforce_csrf_checks=False,
                         raise_request_exception=True, **defaults)
        self.handler = CustomClientHandler(enforce_csrf_checks)


class BaseTestsWithCookies(BaseTests):
    def setUp(self):
        super(BaseTestsWithCookies, self).setUp()
        self.user = new_random_user()
        self.client = CustomAPIClient()


class AuthTests(BaseTestsWithCookies):
    def check_auth(self, jwt_token: str, refresh_token: str = '',
                   headers: dict = None, success: bool = False):
        headers = headers or {}
        resp = self.client.get(
            reverse('users-list'),
            COOKIES={
                JWT_AUTH_COOKIE: jwt_token,
                JWT_AUTH_REFRESH_COOKIE: refresh_token
            },
            **headers
        )
        if success:
            self.assertEqual(resp.status_code, status.HTTP_200_OK)
        else:
            self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)

    @patch('orea.authentication.conf_auth')
    def test_auth_with_cookie(self, mock_conf_auth: MagicMock):
        test_raw_token = "raw_token"
        test_validated_token = "validated"
        mock_conf_auth.validate_token.return_value = test_validated_token

        self.check_auth(test_raw_token, success=True)

        mock_conf_auth.validate_token.assert_called_once_with(test_raw_token)
        mock_conf_auth.get_user.assert_called_once_with(test_validated_token)

    @patch('orea.authentication.conf_auth')
    def test_auth_with_header(self, mock_conf_auth: MagicMock):
        test_raw_token = "raw_token"
        test_header_raw_token = "header_raw_token"
        test_validated_token = "validated"
        mock_conf_auth.validate_token.return_value = test_validated_token

        self.check_auth(test_raw_token, headers={
            api_settings.AUTH_HEADER_NAME: f"Bearer {test_header_raw_token}"
        }, success=True)

        mock_conf_auth.validate_token.assert_called_once_with(
            test_header_raw_token.encode('ascii')
        )
        mock_conf_auth.get_user.assert_called_once_with(test_validated_token)

    @patch('orea.authentication.conf_auth')
    def test_auth_with_cookie_validated_not_implemented(
            self, mock_conf_auth: MagicMock
    ):
        test_raw_token = jwt_encode(self.user)[0].__str__()
        test_validated_token = AccessToken(test_raw_token)
        mock_conf_auth.validate_token.side_effect = NotImplementedError()
        mock_conf_auth.get_user.return_value = self.user

        self.check_auth(test_raw_token, success=True)

        mock_conf_auth.validate_token.assert_called_once_with(test_raw_token)
        mock_arg = mock_conf_auth.get_user.call_args.args[0]
        [self.assertEqual(
            getattr(test_validated_token, k), getattr(mock_arg, k),
            msg=f"with {k}"
        ) for k in mock_arg.__dict__ if k != 'current_time']
        mock_conf_auth.get_user.assert_called_once()

    @patch('orea.authentication.conf_auth')
    def test_auth_with_cookie_get_user_not_implemented(
            self, mock_conf_auth: MagicMock
    ):
        test_raw_token = "raw_token"
        test_validated_token = AccessToken(jwt_encode(self.user)[0].__str__())
        mock_conf_auth.validate_token.return_value = test_validated_token
        mock_conf_auth.get_user.side_effect = NotImplementedError()

        self.check_auth(test_raw_token, success=True)

        mock_conf_auth.validate_token.assert_called_once_with(test_raw_token)
        mock_conf_auth.get_user.assert_called_once_with(test_validated_token)

    @patch('orea.authentication.conf_auth')
    def test_auth_with_cookie_auth_not_implemented(
            self, mock_conf_auth: MagicMock
    ):
        test_raw_token = jwt_encode(self.user)[0].__str__()
        test_validated_token = AccessToken(test_raw_token)
        mock_conf_auth.validate_token.side_effect = NotImplementedError()
        mock_conf_auth.get_user.side_effect = NotImplementedError()

        self.check_auth(test_raw_token, success=True)

        mock_conf_auth.validate_token.assert_called_once_with(test_raw_token)
        mock_arg = mock_conf_auth.get_user.call_args.args[0]
        [self.assertEqual(
            getattr(test_validated_token, k), getattr(mock_arg, k),
            msg=f"with {k}"
        ) for k in mock_arg.__dict__ if k != 'current_time']
        mock_conf_auth.get_user.assert_called_once()


class RefreshTests(BaseTestsWithCookies):
    def check_refresh(self, jwt_token: str, refresh_token: str = '',
                      headers: dict = None) -> Response:
        headers = headers or {}
        resp = self.client.post(
            reverse('token_refresh'),
            COOKIES={
                JWT_AUTH_COOKIE: jwt_token,
                JWT_AUTH_REFRESH_COOKIE: refresh_token
            },
            **headers
        )
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        return resp

    @patch('orea.serializers.conf_auth')
    def test_refresh(self, mock_conf_auth: MagicMock):
        test_refresh_token = "refresh"
        test_access_new_token = "new_access"
        test_refresh_new_token = "new_refresh"

        mock_conf_auth.refresh_jwt.return_value = JwtTokens(
            access=test_access_new_token,
            refresh=test_refresh_new_token,
        )

        resp = self.check_refresh('', test_refresh_token)

        mock_conf_auth.refresh_jwt.assert_called_once_with(test_refresh_token)
        self.assertIn('access', resp.cookies)
        self.assertIn('refresh', resp.cookies)
        self.assertIn(resp.cookies['access'].value, test_access_new_token)
        self.assertIn(resp.cookies['refresh'].value, test_refresh_new_token)

    @patch('orea.serializers.conf_auth')
    @patch.object(TokenRefreshSerializer, 'validate')
    def test_refresh_not_implemented(self, mock_serializer_validate: MagicMock,
                                     mock_conf_auth: MagicMock):
        test_refresh_token = "refresh"
        test_access_new_token = "new_access"
        test_refresh_new_token = "new_refresh"

        mock_conf_auth.refresh_jwt.side_effect = NotImplementedError()
        mock_serializer_validate.return_value = {
            'access': test_access_new_token,
            'refresh': test_refresh_new_token
        }

        resp = self.check_refresh('', test_refresh_token)

        self.assertEqual(mock_serializer_validate.call_args.args[0]['refresh'],
                         test_refresh_token)
        self.assertIn('access', resp.cookies)
        self.assertIn('refresh', resp.cookies)
        self.assertIn(resp.cookies['access'].value, test_access_new_token)
        self.assertIn(resp.cookies['refresh'].value, test_refresh_new_token)


# Users tests

class UserCase:
    def __init__(self, authorized_reas: List[IntensiveCareService]):
        self.authorized_reas = authorized_reas


class UserPatchCase(PatchCase, UserCase):
    def __init__(self, authorized_reas: List[IntensiveCareService], **kwargs):
        super(UserPatchCase, self).__init__(**kwargs)
        UserCase.__init__(self, authorized_reas)


class UserTests(ViewSetTestsWithBasicInfra):
    unupdatable_fields = ["uuid", "date_joined"]
    unsettable_default_fields = dict()
    unsettable_fields = []

    objects_url = "users"
    retrieve_view = UserViewSet.as_view({'get': 'retrieve'})
    list_view = UserViewSet.as_view({'get': 'list'})
    create_view = UserViewSet.as_view({'post': 'create'})
    delete_view = UserViewSet.as_view({'delete': 'destroy'})
    update_view = UserViewSet.as_view({'patch': 'partial_update'})
    model = User
    model_objects = User.objects
    model_fields = User._meta.fields


class UserListTests(UserTests):
    def setUp(self):
        from users.models import Access

        super(UserListTests, self).setUp()
        self.name_pattern = random_str(3).replace(' ', 'e')
        self.test_date = timezone.now() - timedelta(days=100)
        nb_users = 500

        self.admin_user: User = new_random_user("admin", "main", is_staff=True)

        def random_str_value(perfect_match_possible: bool = False):
            return random_str(
                random.randint(5, 7),
                include_pattern=(self.name_pattern
                                 if random.random() > 0.8 else "")
            ) if (not perfect_match_possible or random.random()) > 0.2\
                else self.name_pattern

        self.list_users: List[User] = User.objects.bulk_create([
            User(
                uuid=uuid4(),
                title=random.choice(User.Title.values),
                username=str(i),
                first_name=random_str_value(perfect_match_possible=True),
                last_name=random_str_value(perfect_match_possible=True),
                email=f"{random_str_value(perfect_match_possible=True)}@ml.com",
                is_staff=random.random() > 0.5,
                is_active=random.random() > 0.5,
                date_joined=self.test_date + timedelta(
                    days=random.choice(range(-10, 10))
                ),
            ) for i in range(nb_users)
        ])

        self.list_acc: List[Access] = Access.objects.bulk_create(
            [Access(
                intensive_care_service=random.choice([
                    self.serv_1, self.serv_2
                ]),
                user=u,
                start_datetime=timezone.now() - timedelta(days=1),
                end_datetime=timezone.now() + timedelta(days=1),
                status=random.choice(Access.Status.values),
                reviewed_by=self.admin_user,
                right_edit=True,
                right_review=True,
            ) for u in self.list_users]
        )
        self.list_users.append(self.admin_user)

        self.basic_case = ListCase(
            to_find=self.list_users,
            success=True, status=status.HTTP_200_OK,
            user=self.admin_user
        )

    def test_get_all_as_admin(self):
        self.check_get_paged_list_case(self.basic_case)

    def test_get_users_from_same_service(self):
        from users.models import Access

        self.admin_user.is_staff = False
        self.admin_user.save()
        acc: Access = Access.objects.create(
            intensive_care_service=self.serv_1,
            user=self.admin_user,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            reviewed_by=self.admin_user,
            right_edit=True,
            right_review=True,
        )
        self.check_get_paged_list_case(self.basic_case.clone(
            to_find=list(set([
                a.user for a in self.list_acc + [acc]
                if a.intensive_care_service == self.serv_1
            ])),
        ))


class UserPatchTests(UserTests):
    def setUp(self):
        from users.models import Access

        super(UserPatchTests, self).setUp()
        self.admin_user: User = new_random_user("admin", "user", is_staff=True)
        self.user_1: User = new_random_user("user", "1", is_staff=False)
        self.user_2: User = new_random_user("user", "2", is_staff=False)

        Access.objects.create(
            intensive_care_service=self.serv_1,
            user=self.user_1,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            reviewed_by=self.admin_user,
            right_edit=True,
            right_review=True,
        )
        Access.objects.create(
            intensive_care_service=self.serv_2,
            user=self.user_2,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            reviewed_by=self.admin_user,
            right_edit=True,
            right_review=True,
        )

        self.test_str = "test"
        self.test_email = "test@test.com"
        self.test_date = timezone.now() - timedelta(days=1)

        self.initial_data = {
            'uuid': uuid4(),
            'title': User.Title.care_giver,
            'username': self.test_str,
            'first_name': self.test_str,
            'last_name': self.test_str,
            'email': self.test_email,
            'is_staff': False,
            'is_active': False,
            'date_joined': self.test_date,
        }
        self.base_data_to_update = {
            'uuid': uuid4(),
            'title': User.Title.care_giver,
            'username': 'updated',
            'first_name': 'updated',
            'last_name': 'updated',
            'email': 'updated@updated.com',
            'is_staff': True,
            'is_active': True,
            'date_joined': timezone.now(),
        }
        self.basic_patch_case = UserPatchCase(
            initial_data=self.initial_data,
            authorized_reas=[self.serv_1],
            data_to_update=self.base_data_to_update,
            user=self.admin_user, status=status.HTTP_200_OK, success=True,
        )

    def check_self_patch_case(self, case: PatchCase):
        obj, created = self.model_objects.update_or_create(**case.initial_data)

        request = self.factory.patch(
            self.objects_url, case.data_to_update, format='json'
        )
        force_authenticate(request, obj)
        super().check_patch_case(case, obj=obj, request=request)

    def init_case_obj(self, case: UserPatchCase):
        obj_id = self.model_objects.create(**case.initial_data).pk
        obj = self.model_objects.get(pk=obj_id)

        Access.objects.bulk_create(
            Access(
                start_datetime=timezone.now() - timedelta(days=1),
                end_datetime=timezone.now() + timedelta(days=1),
                user=obj,
                intensive_care_service=s,
                status=Access.Status.validated,
                reviewed_by=self.admin_user,
                right_edit=True,
                right_review=True,
            ) for s in case.authorized_reas)
        return obj

    def check_patch_case(self, case: UserPatchCase, obj: Model = None):
        obj: User = self.init_case_obj(case)
        return super().check_patch_case(case, obj)

    def test_patch_as_admin(self):
        self.check_patch_case(self.basic_patch_case)

    def test_err_patch_as_non_admin(self):
        self.check_patch_case(self.basic_patch_case.clone(
            data_to_update={
                'username': 'updated',
                'first_name': 'updated',
                'last_name': 'updated',
                'email': 'updated@updated.com',
            },
            user=self.user_1, success=False,
            status=status.HTTP_403_FORBIDDEN
        ))

    def test_err_patch_in_other_service_as_non_admin(self):
        self.check_patch_case(self.basic_patch_case.clone(
            data_to_update={
                'username': 'updated',
                'first_name': 'updated',
                'last_name': 'updated',
                'email': 'updated@updated.com',
            },
            authorized_reas=[self.serv_2],
            user=self.user_1, success=False,
            status=status.HTTP_404_NOT_FOUND
        ))

    def test_patch_self(self):
        self.check_self_patch_case(self.basic_patch_case.clone(
            data_to_update={
                'username': 'updated',
                'first_name': 'updated',
                'last_name': 'updated',
                'email': 'updated@updated.com',
            }
        ))

    def test_err_patch_self_non_admin_unauthorised_fields(self):
        [self.check_self_patch_case(self.basic_patch_case.clone(
            data_to_update=d,
            success=False, status=status.HTTP_200_OK
        )) for d in [
            {'title': User.Title.care_giver},
            {'is_staff': True},
            {'is_active': True},
        ]]
