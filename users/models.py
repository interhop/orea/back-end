from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError

from orea.models import BaseModel, BaseTextChoices


class Access(BaseModel):
    class Status(BaseTextChoices):
        created = "created", _("created")
        validated = "validated", _("validated")
        denied = "denied", _("denied")

    intensive_care_service = models.ForeignKey(
        "infra.IntensiveCareService", related_name="accesses",
        on_delete=models.CASCADE, null=False)
    user = models.ForeignKey(
        "orea.User", related_name="accesses",
        on_delete=models.CASCADE, null=False)
    start_datetime = models.DateTimeField(null=False)
    end_datetime = models.DateTimeField()
    status = models.CharField(choices=Status.choices, default=Status.created,
                              max_length=Status.max_length())
    reviewed_by = models.ForeignKey(
        "orea.User", related_name="accesses_reviewed",
        on_delete=models.DO_NOTHING, null=True)
    right_edit = models.BooleanField(default=False)
    right_review = models.BooleanField(default=False)

    def check_reviewable(self):
        if self.status != self.Status.created:
            raise ValidationError(
                _(f"Access status can be updated only if current status is "
                  f"'{self.Status.created}'. Current status is "
                  f"'{self.status}'"))

    def validate(self):
        if self.end_datetime < timezone.now():
            raise ValidationError(
                _("Access status cannot be updated after it ended."))

        if self.start_datetime < timezone.now():
            self.check_reviewable()
            self.start_datetime = timezone.now()

        self.status = self.Status.validated
        self.save()

    def deny(self):
        if self.end_datetime < timezone.now():
            raise ValidationError(
                _("Access status cannot be updated after it ended."))

        if self.start_datetime < timezone.now():
            self.check_reviewable()

        self.status = self.Status.denied
        self.save()

    def close(self):
        if self.start_datetime > timezone.now():
            raise ValidationError(
                _(f"Cannot close an access that has not started"
                  f" (start is {self.start_datetime})"))
        if self.end_datetime < timezone.now():
            raise ValidationError(
                _(f"Cannot close an access that is already finished"
                  f" (end is {self.end_datetime})"))

        if self.status != self.Status.validated:
            raise ValidationError(
                _(f"Cannot close an access not validated "
                  f"(status is '{self.status}')"))
        self.end_datetime = timezone.now()
        self.save()

    @property
    def is_valid(self):
        # todo : test
        t = timezone.now()
        return ((self.start_datetime < t < self.end_datetime)
                and self.status == self.Status.validated)

    @property
    def started(self):
        t = timezone.now()
        return self.start_datetime < t

    @classmethod
    def is_valid_Q(cls, prefix: str = "") -> Q:
        prefix = f"{prefix}__" if prefix else ""
        t = timezone.now()
        return Q(**{
            f'{prefix}start_datetime__lte': t,
            f'{prefix}end_datetime__gte': t,
            f'{prefix}status': cls.Status.validated,
        })

    def __str__(self):
        date_format = '%d-%m-%Y'
        return f"[{self.status}] {self.user} " \
               f"(Service {self.intensive_care_service}) - " \
               f"{self.start_datetime.date().strftime(date_format)} " \
               f"-> {self.end_datetime.date().strftime(date_format)}"


class PersonalNote(BaseModel):
    owner = models.ForeignKey("orea.User", related_name="notes",
                              on_delete=models.CASCADE, null=False)
    content = models.TextField()

    class Meta:
        unique_together = [("owner",)]
