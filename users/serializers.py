from datetime import timedelta

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from orea.models import User
from orea.serializers import BaseSerializer
from .models import Access


class PeriodValidator:
    message = _('start_datetime must be anterior to end_datetime')
    requires_context = True

    def __call__(self, attrs, serializer):
        start = attrs.get('start_datetime', None)
        end = attrs.get('end_datetime', None)
        if start is None or end is None:
            return

        if start > end:
            raise ValidationError(self.message)


class AccessSerializer(BaseSerializer):
    status = serializers.CharField(read_only=True)
    reviewed_by = serializers.PrimaryKeyRelatedField(read_only=True)
    # with auto_now_add, default serializer is read_only
    start_datetime = serializers.DateTimeField(required=False)
    end_datetime = serializers.DateTimeField(required=False)

    def get_validators(self):
        return [*super(AccessSerializer, self).get_validators(),
                PeriodValidator()]

    def validate_start_datetime(self, value):
        if value is not None:
            if value < (timezone.now() - timedelta(minutes=1)):
                raise ValidationError('start_datetime cannot be in the past')
        return value

    def validate_end_datetime(self, value):
        if value is not None:
            if value < (timezone.now() - timedelta(minutes=1)):
                raise ValidationError('end_datetime cannot be in the past')
        return value

    class Meta:
        model = Access
        fields = "__all__"

    def create(self, validated_data):
        start = validated_data.get('start_datetime', None)
        end = validated_data.get('end_datetime', None)

        access_user = validated_data.get('user', None)
        if access_user:
            service = validated_data.get('intensive_care_service')
            user: User = self.context.get("request", None).user
            authorized_service = user.authorized_intensive_care_services\
                .filter(pk=service.pk).first()
            if (access_user.pk != user.pk and not user.is_staff
                    and not authorized_service):
                raise ValidationError(_(
                    'Cannot create access for other users if you are not admin'
                ))

        if start is None:
            validated_data['start_datetime'] = timezone.now()
        if end is None:
            validated_data['end_datetime'] = (start if start is not None
                                              else timezone.now()
                                              ) + timedelta(days=182)
        return super(AccessSerializer, self).create(validated_data)
