import random
import uuid
from datetime import timedelta
from itertools import product
from typing import List, Union

from django.utils import timezone
from django.utils.datetime_safe import datetime
from rest_framework import status
from rest_framework.test import force_authenticate

from infra.tests.tools import ViewSetTestsWithBasicInfra
from .models import Access
from .views import AccessViewSet
from orea.models import User
from orea.tests_tools import new_random_user, RequestCase, CreateCase, \
    CaseRetrieveFilter, random_str, ListCase, PatchCase,\
    DeleteCase
from orea.utils import prettify_json

INFRA_URL = "/users-app"


class AccessTests(ViewSetTestsWithBasicInfra):
    unupdatable_fields = ['intensive_care_service', 'user', 'reviewed_by',
                          'right_edit', 'right_review']
    unsettable_default_fields = dict(status=Access.Status.created)
    unsettable_fields = ['reviewed_by']

    objects_url = f"{INFRA_URL}/accesses"
    retrieve_view = AccessViewSet.as_view({'get': 'retrieve'})
    list_view = AccessViewSet.as_view({'get': 'list'})
    create_view = AccessViewSet.as_view({'post': 'create'})
    delete_view = AccessViewSet.as_view({'delete': 'destroy'})
    update_view = AccessViewSet.as_view({'patch': 'partial_update'})
    model = Access
    model_objects = Access.objects
    model_fields = Access._meta.fields

    def setUp(self):
        super(AccessTests, self).setUp()

        # Users
        self.admin_user: User = new_random_user("admin", "main", is_staff=True)
        self.user_1_serv1_admin: User = new_random_user("admin", "h1")
        self.user_2_serv1_simple: User = new_random_user("simple", "h1")
        self.user_3_serv2_admin: User = new_random_user("admin", "h2")
        self.user_no_access: User = new_random_user("access", "no")

        self.user_1_access: Access = Access.objects.create(
            intensive_care_service=self.serv_1,
            user=self.user_1_serv1_admin,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=True,
            right_review=True
        )
        self.user_2_access: Access = Access.objects.create(
            intensive_care_service=self.serv_1,
            user=self.user_2_serv1_simple,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=True,
            right_review=False
        )
        self.user_3_access: Access = Access.objects.create(
            intensive_care_service=self.serv_2,
            user=self.user_3_serv2_admin,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=True,
            right_review=True
        )


class AccessCaseRetrieveFilter(CaseRetrieveFilter):
    def __init__(self, user: str, exclude: dict = None):
        self.user = user
        super(AccessCaseRetrieveFilter, self).__init__(exclude=exclude)


class DatesAccessCaseRetrieveFilter(CaseRetrieveFilter):
    def __init__(self, user: str, start_datetime: datetime,
                 end_datetime: datetime, exclude: dict = None):
        self.user = user
        self.start_datetime = start_datetime
        self.end_datetime = end_datetime
        super(DatesAccessCaseRetrieveFilter, self).__init__(exclude=exclude)

    @property
    def args(self) -> dict:
        return dict(
            user=self.user,
            start_datetime__lte=self.start_datetime + timedelta(seconds=1),
            start_datetime__gte=self.start_datetime - timedelta(seconds=1),
            end_datetime__lte=self.end_datetime + timedelta(seconds=1),
            end_datetime__gte=self.end_datetime - timedelta(seconds=1),
        )


class AccessGetListTests(AccessTests):
    def setUp(self):
        super(AccessGetListTests, self).setUp()
        self.name_pattern = random_str(3).replace(' ', 'e')
        nb_acces = 500

        self.user_test_1: User = new_random_user()
        self.user_test_2: User = new_random_user()

        self.list_acces: List[Access] = Access.objects.bulk_create([
            Access(
                **{
                    'intensive_care_service': random.choice([self.serv_1,
                                                             self.serv_2]),
                    'user': random.choice([self.user_test_1, self.user_test_2]),
                    'start_datetime': (timezone.now() +
                                       timedelta(days=random.choice([-3, 3]))),
                    'end_datetime': (timezone.now() +
                                     timedelta(days=random.choice([-1, 4]))),
                    'reviewed_by': random.choice([self.user_1_serv1_admin,
                                                  self.admin_user]),
                    'right_edit': random.random() > 0.5,
                    'right_review': random.random() > 0.5,
                    'status': random.choice([
                        s for s in Access.Status.values
                        if s != Access.Status.validated.value
                    ]),
                }
            ) for _ in range(nb_acces)
        ]) + [
                                            self.user_1_access,
                                            self.user_2_access,
                                            self.user_3_access
                                        ]

    def test_get_all_my_acces_without_right(self):
        # As a user with no right, I can get all of my access
        case = ListCase(
            to_find=[a for a in self.list_acces
                     if a.user.pk == self.user_test_1.pk],
            success=True,
            status=status.HTTP_200_OK,
            user=self.user_test_1
        )
        self.check_get_paged_list_case(case)

    def test_get_all_acces_as_admin(self):
        # As a user with is_staff=True, I can get all access
        case = ListCase(
            to_find=self.list_acces,
            success=True,
            status=status.HTTP_200_OK,
            user=self.admin_user
        )
        self.check_get_paged_list_case(case)

    def test_get_acces_as_service_admin(self):
        # As a user with review access to a service,
        # I can get all accesses from this service
        case = ListCase(
            to_find=[a for a in self.list_acces
                     if a.user.pk == self.user_1_serv1_admin.pk
                     or a.intensive_care_service.pk == self.serv_1.pk],
            success=True,
            status=status.HTTP_200_OK,
            user=self.user_1_serv1_admin
        )
        self.check_get_paged_list_case(case)

    def test_get_list_with_params_1(self):
        # As a user with is_staff=True, I can get acces filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin_user)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"search={self.user_2_serv1_simple.first_name}",
                to_find=[
                    acc for acc in self.list_acces
                    if acc.user.pk == self.user_2_serv1_simple.pk],
                params=dict(search=self.user_2_serv1_simple.first_name)
            )
        )

    def test_get_list_with_params_2(self):
        # As a user with is_staff=True, I can get acces filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin_user)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"search={self.user_3_serv2_admin.last_name}",
                to_find=[
                    acc for acc in self.list_acces
                    if acc.user.pk == self.user_3_serv2_admin.pk],
                params=dict(search=self.user_3_serv2_admin.last_name)
            )
        )

    def test_get_list_with_params_3(self):
        # As a user with is_staff=True, I can get acces filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin_user)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"search={self.user_1_serv1_admin.username}",
                to_find=[
                    acc for acc in self.list_acces
                    if acc.user.pk == self.user_1_serv1_admin.pk],
                params=dict(search=self.user_1_serv1_admin.username)
            )
        )

    def test_get_list_with_params_4(self):
        # As a user with is_staff=True, I can get acces filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin_user)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"search={self.hosp.name}",
                to_find=[
                    acc for acc in self.list_acces
                    if acc.intensive_care_service.hospital.pk == self.hosp.pk],
                params=dict(search=self.hosp.name)
            )
        )

    def test_get_list_with_params_5(self):
        # As a user with is_staff=True, I can get acces filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin_user)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"search={self.serv_1.name}",
                to_find=[
                    acc for acc in self.list_acces
                    if acc.intensive_care_service.pk == self.serv_1.pk],
                params=dict(search=self.serv_1.name)
            )
        )

    def test_get_list_with_params_6(self):
        # As a user with is_staff=True, I can get acces filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin_user)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"intensive_care_service={self.serv_1}",
                to_find=[
                    acc for acc in self.list_acces
                    if acc.intensive_care_service.pk == self.serv_1.pk],
                params=dict(intensive_care_service=self.serv_1.pk)
            )
        )

    def test_get_list_with_params_7(self):
        # As a user with is_staff=True, I can get acces filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin_user)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"user={self.user_test_1}",
                to_find=[
                    acc for acc in self.list_acces
                    if acc.user.pk == self.user_test_1.pk],
                params=dict(user=self.user_test_1.pk)
            )
        )

    def test_get_list_with_params_8(self):
        # As a user with is_staff=True, I can get acces filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin_user)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"status={Access.Status.validated}",
                to_find=[
                    acc for acc in self.list_acces
                    if acc.status == Access.Status.validated],
                params=dict(status=Access.Status.validated)
            )
        )

    def test_get_list_with_params_9(self):
        # As a user with is_staff=True, I can get acces filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin_user)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"reviewed_by={self.admin_user}",
                to_find=[
                    acc for acc in self.list_acces
                    if (acc.reviewed_by
                        and acc.reviewed_by.pk == self.admin_user.pk)],
                params=dict(reviewed_by=self.admin_user.pk)
            )
        )

    def test_get_list_with_params_10(self):
        # As a user with is_staff=True, I can get acces filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin_user)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title="right_edit=True",
                to_find=[
                    acc for acc in self.list_acces
                    if acc.right_edit],
                params=dict(right_edit=True)
            )
        )

    def test_get_list_with_params_11(self):
        # As a user with is_staff=True, I can get acces filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin_user)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title="right_review=True",
                to_find=[acc for acc in self.list_acces if acc.right_review],
                params=dict(right_review=True)
            )
        )

    def test_error_not_authenticated(self):
        # As a user not authenticated, I cannot get any acces
        case = ListCase(
            to_find=[],
            success=False,
            status=status.HTTP_401_UNAUTHORIZED
        )
        self.check_get_paged_list_case(case)


class AccessCreateTests(AccessTests):
    def setUp(self):
        super(AccessCreateTests, self).setUp()

        self.creation_data = {
            'intensive_care_service': self.serv_1.pk,
            'user': self.user_no_access.pk,
            'start_datetime': timezone.now(),
            'end_datetime': timezone.now() + timedelta(days=7),
            'right_edit': True,
            'right_review': False,
        }
        self.basic_create_case = CreateCase(
            data=self.creation_data,
            retrieve_filter=AccessCaseRetrieveFilter(
                user=self.user_no_access.pk),
            user=None, status=None, success=None,
        )

    def test_create_as_simple_user(self):
        # As a user with no access, I can create a new acc
        case = self.basic_create_case.clone(
            user=self.user_no_access,
            success=True,
            status=status.HTTP_201_CREATED,
        )
        self.check_create_case(case)

    def test_create_for_other_user_as_admin(self):
        # As a user with is_staff=True, I can create a new acc
        case = self.basic_create_case.clone(
            user=self.admin_user,
            success=True,
            status=status.HTTP_201_CREATED,
        )
        self.check_create_case(case)

    def test_create_for_other_user_as_service_admin(self):
        # As a user with admin access to a service, I can create a new acc
        case = self.basic_create_case.clone(
            user=self.user_1_serv1_admin,
            success=True,
            status=status.HTTP_201_CREATED,
        )
        self.check_create_case(case)

    def test_create_with_none_values(self):
        # As a user with no access, I can create a new acc, and if I don't
        # specify start and end datetime, defaults will be set
        case = self.basic_create_case.clone(
            data={
                'intensive_care_service': self.serv_1.pk,
                'user': self.user_no_access.pk,
                'right_edit': True,
                'right_review': False,
            },
            retrieve_filter=DatesAccessCaseRetrieveFilter(
                user=self.user_no_access.pk,
                start_datetime=timezone.now(),
                end_datetime=timezone.now() + timedelta(days=182),
            ),
            user=self.user_no_access,
            success=True,
            status=status.HTTP_201_CREATED,
        )
        self.check_create_case(case)

    def test_error_create_for_another_user(self):
        # As a user with no access, I cannot create a new acc for another user
        case = self.basic_create_case.clone(
            user=self.user_no_access,
            data={**self.creation_data, 'user': self.user_2_serv1_simple.pk},
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
        )
        self.check_create_case(case)

    def test_create_with_unsettable_values(self):
        # As a user with is_staff=False, I cannot create a new acc
        cases = [self.basic_create_case.clone(
            user=u,
            success=True,
            status=status.HTTP_201_CREATED,
            data={
                **self.creation_data,
                'status': Access.Status.validated,
                'reviewed_by': self.user_2_serv1_simple.pk,
            }
        ) for u in [self.user_no_access, self.user_1_serv1_admin,
                    self.admin_user]
        ]
        [self.check_create_case(case) for case in cases]

    def test_err_create_with_wrong_data(self):
        # Even as a user with is_staff=True,
        # asking for some fields will return default
        cases = [self.basic_create_case.clone(
            user=self.admin_user,
            data={**self.creation_data, **d},
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
        ) for d in [
            dict(start_datetime=timezone.now() - timedelta(days=1)),
            dict(end_datetime=timezone.now() - timedelta(days=1)),
            dict(
                start_datetime=timezone.now() + timedelta(days=2),
                end_datetime=timezone.now() + timedelta(days=1)
            ),
            dict(user=str(uuid.uuid4())),
            dict(intensive_care_service=str(uuid.uuid4()))
        ]]
        [self.check_create_case(case) for case in cases]


class AccessPatchTests(AccessTests):
    def setUp(self):
        super(AccessPatchTests, self).setUp()

        self.created_data = {
            'intensive_care_service': self.serv_1,
            'user': self.user_no_access,
            'start_datetime': timezone.now() + timedelta(days=1),
            'end_datetime': timezone.now() + timedelta(days=7),
            'right_edit': True,
            'right_review': False,
            'status': Access.Status.created,
            'reviewed_by': None,
        }
        self.basic_patch_case = PatchCase(
            initial_data=self.created_data,
            data_to_update={},
            valid=False,
            user=None, status=None, success=None,
        )

    def test_err_patch_as_admin(self):
        # As any user, I cannot edit an access
        case = self.basic_patch_case.clone(
            user=self.admin_user,
            success=False,
            status=status.HTTP_405_METHOD_NOT_ALLOWED,
        )
        self.check_patch_case(case)

    # def test_patch_as_serv_admin(self):
    #     # As a user with is_staff=True, I can edit a acc
    #     case = self.basic_patch_case.clone(
    #         user=self.user_1_serv1_admin,
    #         success=True,
    #         status=status.HTTP_200_OK,
    #     )
    #     self.check_patch_case(case)
    #
    # def test_err_patch_as_other_serv_admin(self):
    #     # As a user with is_staff=True, I can edit a acc
    #     case = self.basic_patch_case.clone(
    #         user=self.user_3_serv2_admin,
    #         success=True,
    #         status=status.HTTP_404_NOT_FOUND,
    #     )
    #     self.check_patch_case(case)
    #
    # def test_err_patch_as_simple_user(self):
    #     # As a user with is_staff=True, I can edit a acc
    #     case = self.basic_patch_case.clone(
    #         user=self.user_2_serv1_simple,
    #         success=True,
    #         status=status.HTTP_404_NOT_FOUND,
    #     )
    #     self.check_patch_case(case)
    #
    # def test_patch_as_concerned_simple_user(self):
    #     # As a user with no access, I can edit an access I created
    #     case = self.basic_patch_case.clone(
    #         user=self.user_no_access,
    #         data_to_update={
    #             'start_datetime': timezone.now() + timedelta(days=2),
    #             'end_datetime': timezone.now() + timedelta(days=8),
    #         },
    #         success=True,
    #         status=status.HTTP_200_OK,
    #     )
    #     self.check_patch_case(case)
    #
    # def test_err_patch_status_as_concerned_simple_user(self):
    #     # As a user with no access,
    #     # I cannot edit status on an access I created
    #     cases = [self.basic_patch_case.clone(
    #         user=self.user_no_access,
    #         data_to_update={'status': s},
    #         success=True,
    #         status=status.HTTP_403_FORBIDDEN,
    #     ) for s in Access.Status.values]
    #
    #     [self.check_patch_case(case) for case in cases]
    #
    # def test_err_patch_wrong_data(self):
    #     # As a user with is_staff=true,
    #     # I cannot edit with unacceptable data
    #     cases = [self.basic_patch_case.clone(
    #         user=self.admin_user,
    #         data_to_update=d,
    #         success=True,
    #         status=status.HTTP_403_FORBIDDEN,
    #     ) for d in [
    #         dict(start_datetime=timezone.now() - timedelta(days=1)),
    #         dict(end_datetime=timezone.now() - timedelta(days=1)),
    #         dict(
    #             start_datetime=timezone.now() + timedelta(days=2),
    #             end_datetime=timezone.now() + timedelta(days=1)
    #         ),
    #         dict(user=self.user_2_serv1_simple),  # unupdatable
    #         dict(right_edit=False),  # unupdatable
    #         dict(right_review=True),  # unupdatable
    #         dict(reviewed_by=self.admin_user.pk),  # unupdatable
    #         dict(intensive_care_service=self.serv_2)  # unupdatable
    #     ]]
    #
    #     [self.check_patch_case(case) for case in cases]
    #
    # def test_err_patch_dates_in_conditions(self):
    #     # As a user with is_staff=true,
    #     # I cannot edit the start or end dates of an access in sme situations
    #     cases = [self.basic_patch_case.clone(
    #         user=self.admin_user,
    #         initial_data=i_d,
    #         data_to_update=d_u,
    #         success=True,
    #         status=status.HTTP_400_BAD_REQUEST,
    #     ) for i_d, d_u in [[
    #         # access that already started
    #         {**self.created_data,
    #          'start_datetime': timezone.now() - timedelta(days=2)},
    #         dict(start_datetime=timezone.now() - timedelta(days=1))
    #     ], [
    #         # access created but end_date in past
    #         {**self.created_data,
    #          'start_datetime': timezone.now() - timedelta(days=2)},
    #         dict(end_datetime=timezone.now() - timedelta(days=1))
    #     ], [
    #         # access that already ended
    #         {**self.created_data,
    #          'end_datetime': timezone.now() - timedelta(days=2)},
    #         dict(start_datetime=timezone.now() + timedelta(days=5))
    #     ], [
    #         # access that already ended
    #         {**self.created_data,
    #          'end_datetime': timezone.now() - timedelta(days=2)},
    #         dict(end_datetime=timezone.now() + timedelta(days=5))
    #     ]]]
    #
    #     [self.check_patch_case(case) for case in cases]
    #
    # def test_patch_end_date_when_started(self):
    #     # As a user with is_staff=true,
    #     # I can edit the start or end dates of an access even when it started
    #     case = self.basic_patch_case.clone(
    #         user=self.admin_user,
    #         initial_data={**self.created_data,
    #                   'start_datetime': timezone.now() - timedelta(days=2)},
    #         data_to_update=dict(end_datetime=timezone.now()),
    #         success=True,
    #         status=status.HTTP_200_OK,
    #     )
    #
    #     self.check_patch_case(case)
    #
    # def test_error_patch_as_basic_user(self):
    #     # As a user with is_staff=False, I cannot edit a acc
    #     case = self.basic_patch_case.clone(
    #         user=self.user_no_access,
    #         success=False,
    #         status=status.HTTP_403_FORBIDDEN,
    #     )
    #     self.check_patch_case(case)


class ReviewCase(PatchCase):
    """
    General representation of a test case for Updating partially an object
    """
    def __init__(self, initial_data: dict, valid: bool, **kwargs):
        super(ReviewCase, self).__init__(
            **{
                **kwargs,
                'initial_data': initial_data,
                'data_to_update': dict(
                    status=(Access.Status.validated if valid
                            else Access.Status.denied)
                ),
            }
        )
        self.valid = valid


class AccessReviewTests(AccessTests):
    deny_view = AccessViewSet.as_view({'patch': 'deny'})
    validate_view = AccessViewSet.as_view({'patch': 'validate'})

    def check_review_case(self, case: ReviewCase) -> Union[Access, None]:
        obj_id = self.model_objects.create(**case.initial_data).pk
        obj = self.model_objects.get(pk=obj_id)

        request = self.factory.patch(self.objects_url)
        force_authenticate(request, case.user)
        response = (self.__class__.validate_view(request, pk=obj_id)
                    if case.valid
                    else self.__class__.deny_view(request, pk=obj_id))
        response.render()

        self.assertEqual(
            response.status_code, case.status,
            msg=(f"{case.description}"
                 + (f" -> {prettify_json(response.content)}"
                    if response.content else "")),
        )

        new_obj = self.model_objects.filter(pk=obj_id).first()

        if case.success:
            self.check_is_updated(new_obj, case, obj)
        else:
            self.check_is_not_updated(new_obj, case, obj)
        return new_obj

    def setUp(self):
        super(AccessReviewTests, self).setUp()

        self.created_data = {
            'intensive_care_service': self.serv_1,
            'user': self.user_no_access,
            'start_datetime': timezone.now() + timedelta(days=1),
            'end_datetime': timezone.now() + timedelta(days=7),
            'right_edit': True,
            'right_review': True,
            'status': Access.Status.created,
            'reviewed_by': None,
        }
        self.basic_review_case = ReviewCase(
            initial_data=self.created_data,
            valid=False,
            user=None, status=None, success=None,
        )

    def test_review_as_admin(self):
        # As a user with is_staff=True, I can review an acc
        cases = [self.basic_review_case.clone(
            user=self.admin_user,
            success=True,
            status=status.HTTP_200_OK,
            valid=b
        ) for b in [True, False]]
        [self.check_review_case(case) for case in cases]

    def test_review_as_service_admin(self):
        # As a user with admin access on a service, I can review an acc
        # to this service
        cases = [self.basic_review_case.clone(
            user=self.user_1_serv1_admin,
            success=True,
            status=status.HTTP_200_OK,
            valid=b
        ) for b in [True, False]]
        [self.check_review_case(case) for case in cases]

    def test_err_review_as_service_user(self):
        # As a user with non-admin access to a service, I cannot review an acc
        # to this service
        cases = [self.basic_review_case.clone(
            user=self.user_2_serv1_simple,
            success=False,
            status=status.HTTP_404_NOT_FOUND,
            valid=b
        ) for b in [True, False]]
        [self.check_review_case(case) for case in cases]

    def test_review_access_already_reviewed_not_started(self):
        # As a user with admin access on a service, I cannot review an acc
        # to this service if it has been reviewed already
        cases = [self.basic_review_case.clone(
            initial_data={**self.created_data,
                          'status': s},
            user=self.user_1_serv1_admin,
            success=True,
            status=status.HTTP_200_OK,
            valid=b
        ) for b, s in product([True, False], [Access.Status.validated,
                                              Access.Status.denied])
        ]
        [self.check_review_case(case) for case in cases]

    def test_review_access_already_reviewed_not_started_as_admin(self):
        # As a user with is_staff=True, I cannot review an acc
        # to this service if it has been reviewed already
        cases = [self.basic_review_case.clone(
            initial_data={**self.created_data,
                          'status': s},
            user=self.admin_user,
            success=True,
            status=status.HTTP_200_OK,
            valid=b
        ) for b, s in product([True, False], [Access.Status.validated,
                                              Access.Status.denied])
        ]
        [self.check_review_case(case) for case in cases]

    def test_err_review_access_already_passed(self):
        # As a user with admin access on a service, I cannot review an acc
        # to this service if end_datetime is already passed
        cases = [self.basic_review_case.clone(
            initial_data={**self.created_data,
                          'end_datetime': timezone.now() - timedelta(days=1)},
            user=self.user_1_serv1_admin,
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
            valid=b
        ) for b in [True, False]]
        [self.check_review_case(case) for case in cases]

    def test_err_review_access_already_passed_as_admin(self):
        # As a user with is_staff=True, I cannot review an acc
        # to this service if end_datetime is already passed
        cases = [self.basic_review_case.clone(
            initial_data={**self.created_data,
                          'end_datetime': timezone.now() - timedelta(days=1)},
            user=self.admin_user,
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
            valid=b
        ) for b in [True, False]]
        [self.check_review_case(case) for case in cases]

    def test_validate_access_already_started(self):
        # As a user with admin access on a service, I can review an acc
        # to this service that started but has not ended,
        # and if I validate it, start_datetime will update to now()
        cases = [self.basic_review_case.clone(
            initial_data={**self.created_data,
                          'start_datetime': timezone.now() - timedelta(days=1)},
            user=self.user_1_serv1_admin,
            success=True,
            status=status.HTTP_200_OK,
            valid=b
        ) for b in [True, False]]

        for case in cases:
            new_acc: Access = self.check_review_case(case)
            if case.valid:
                delta = (new_acc.start_datetime - timezone.now())
                self.assertAlmostEqual(delta.total_seconds(), 0, delta=1)

    def test_err_review_access_already_reviewed_and_started_as_admin(self):
        # As a user with is_staff=True, I cannot review an acc
        # to this service if it has been reviewed already
        cases = [self.basic_review_case.clone(
            initial_data={**self.created_data,
                          'start_datetime': timezone.now() - timedelta(days=1),
                          'status': s},
            user=self.admin_user,
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
            valid=b
        ) for b, s in product([True, False], [Access.Status.validated,
                                              Access.Status.denied])
        ]
        [self.check_review_case(case) for case in cases]


class CloseCase(RequestCase):
    """
    General representation of a test case for Updating partially an object
    """
    def __init__(self, initial_data: dict, **kwargs):
        super(CloseCase, self).__init__(**kwargs)
        self.initial_data = initial_data


class AccessCloseTests(AccessTests):
    close_view = AccessViewSet.as_view({'patch': 'close'})

    def check_close_case(self, case: CloseCase):
        acc_id = self.model_objects.create(**case.initial_data).pk
        acc = self.model_objects.get(pk=acc_id)

        request = self.factory.patch(self.objects_url)
        force_authenticate(request, case.user)
        response = self.__class__.close_view(request, pk=acc_id)
        response.render()

        self.assertEqual(
            response.status_code, case.status,
            msg=(f"{case.description}"
                 + (f" -> {prettify_json(response.content)}"
                    if response.content else "")),
        )

        new_acc: Access = self.model_objects.filter(pk=acc_id).first()

        if case.success:
            delta = (new_acc.end_datetime - timezone.now())
            self.assertAlmostEqual(delta.total_seconds(), 0, delta=1)
        else:
            self.assertEqual(
                acc.end_datetime, new_acc.end_datetime
            )

    def setUp(self):
        super(AccessCloseTests, self).setUp()

        self.created_data = {
            'intensive_care_service': self.serv_1,
            'user': self.user_no_access,
            'start_datetime': timezone.now() - timedelta(days=2),
            'end_datetime': timezone.now() + timedelta(days=7),
            'right_edit': True,
            'right_review': False,
            'status': Access.Status.validated,
            'reviewed_by': None,
        }
        self.basic_close_case = CloseCase(
            initial_data=self.created_data,
            valid=False,
            user=None, status=None, success=None,
        )

    def test_close_as_admin(self):
        # As a user with is_staff=True, I can close an acc
        case = self.basic_close_case.clone(
            user=self.admin_user,
            success=True,
            status=status.HTTP_200_OK
        )
        self.check_close_case(case)

    def test_close_as_service_admin(self):
        # As a user with admin access on a service, I can close an acc
        # to this service
        case = self.basic_close_case.clone(
            user=self.user_1_serv1_admin,
            success=True,
            status=status.HTTP_200_OK,
        )
        self.check_close_case(case)

    def test_close_my_access(self):
        # As a user with access on a service, I can close my own acc
        case = self.basic_close_case.clone(
            user=self.user_no_access,
            success=True,
            status=status.HTTP_200_OK,
        )
        self.check_close_case(case)

    def test_err_close_as_service_user(self):
        # As a user with non-admin access to a service, I cannot close an acc
        # to this service
        case = self.basic_close_case.clone(
            user=self.user_2_serv1_simple,
            success=False,
            status=status.HTTP_404_NOT_FOUND
        )
        self.check_close_case(case)

    def test_err_close_access_already_finished(self):
        # As a user with is_staff=True, I cannot close access
        # that has already finished
        case = self.basic_close_case.clone(
            initial_data={**self.created_data,
                          'end_datetime': timezone.now() - timedelta(days=1)},
            user=self.admin_user,
            success=False,
            status=status.HTTP_400_BAD_REQUEST
        )
        self.check_close_case(case)

    def test_err_close_access_not_started(self):
        # As a user with is_staff=True, I cannot close access
        # that has not started yet finished
        case = self.basic_close_case.clone(
            initial_data={**self.created_data,
                          'start_datetime': timezone.now() + timedelta(days=1)},
            user=self.admin_user,
            success=False,
            status=status.HTTP_400_BAD_REQUEST
        )
        self.check_close_case(case)

    def test_err_close_access_not_validated(self):
        # As a user with is_staff=True, I cannot close access
        # that has not started yet finished
        cases = [self.basic_close_case.clone(
            initial_data={**self.created_data,
                          'status': s},
            user=self.admin_user,
            success=False,
            status=status.HTTP_400_BAD_REQUEST
        ) for s in Access.Status.values if s != Access.Status.validated]
        [self.check_close_case(case) for case in cases]


class AccessDeleteTests(AccessTests):
    def setUp(self):
        super(AccessDeleteTests, self).setUp()

        self.created_data = {
            'intensive_care_service': self.serv_1,
            'user': self.user_no_access,
            'right_edit': True,
            'right_review': False,
            'reviewed_by': None,
        }

        # noinspection PyTypeChecker
        self.deletable_accesses = [{
            **self.created_data,
            'status': Access.Status.validated,
            'start_datetime': timezone.now() + timedelta(days=1),
            'end_datetime': timezone.now() + timedelta(days=3)
        }] + [{
            **self.created_data,
            'status': s,
            'start_datetime': timezone.now() + timedelta(days=start_day_delta),
            'end_datetime': timezone.now() + timedelta(days=end_day_delta)
        } for s, (start_day_delta, end_day_delta) in product([
            v for v in Access.Status.values if v != Access.Status.validated
        ], [(1, 3), (-1, 1), (-3, -1)]
        )]

        self.undeletable_accesses = [{
            **self.created_data,
            'status': Access.Status.validated,
            'start_datetime': timezone.now() + timedelta(days=start_day_delta),
            'end_datetime': timezone.now() + timedelta(days=end_day_delta)
        } for (start_day_delta, end_day_delta) in [(-1, 1), (-3, -1)]
        ]

    def test_delete_my_deletable_access(self):
        # As a simple user, I can soft-delete my access
        # if it has not been validated
        cases = [DeleteCase(
            data_to_delete=a,
            user=self.user_no_access,
            success=True,
            status=status.HTTP_204_NO_CONTENT,
        ) for a in self.deletable_accesses]
        [self.check_delete_case(case) for case in cases]

    def test_err_delete_deletable_access_as_service_admin(self):
        # As a user with service admin access, I cannot delete a service
        # access not owned
        cases = [DeleteCase(
            data_to_delete=a,
            user=self.user_1_serv1_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        ) for a in self.deletable_accesses]
        [self.check_delete_case(case) for case in cases]

    def test_delete_deletable_access_as_admin(self):
        # As a user with is_staff=True, I can soft-delete an
        # access if it has not been validated
        cases = [DeleteCase(
            data_to_delete=a,
            user=self.admin_user,
            success=True,
            status=status.HTTP_204_NO_CONTENT,
        ) for a in self.deletable_accesses]
        [self.check_delete_case(case) for case in cases]

    def test_err_delete_undeletale_access_as_any_user(self):
        # As any user, I cannot delete an undeletable access
        cases = [DeleteCase(
            data_to_delete=a,
            user=u,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        ) for a, u in product(self.undeletable_accesses, [
            self.user_no_access, self.user_1_serv1_admin, self.admin_user
        ])]
        [self.check_delete_case(case) for case in cases]

    def test_err_delete_deletable_access_not_owned(self):
        # As any user, I cannot delete deletable access from another user and
        # on a service I don't administrate
        cases = [DeleteCase(
            data_to_delete={**a, 'intensive_care_service': self.serv_2},
            user=self.user_1_serv1_admin,
            success=False,
            status=status.HTTP_404_NOT_FOUND,
        ) for a in self.deletable_accesses]
        [self.check_delete_case(case) for case in cases]
