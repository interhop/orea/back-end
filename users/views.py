from typing import List

from django.db.models import Q
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import MethodNotAllowed

from infra.models import IntensiveCareService
from orea.models import User
from orea.views import BaseViewSet, BaseFilterSet
from .models import Access
from .serializers import AccessSerializer
from .permissions import AccessPermissions
from rest_framework.response import Response


class AccessFilterSet(BaseFilterSet):
    class Meta:
        model = Access
        fields = "__all__"


class AccessViewSet(BaseViewSet):
    queryset = Access.objects.all()
    serializer_class = AccessSerializer
    permission_classes = [AccessPermissions]
    filterset_class = AccessFilterSet
    search_fields = ("user__first_name",
                     "user__last_name",
                     "user__username",
                     "intensive_care_service__hospital__name",
                     "intensive_care_service__name")

    def get_queryset(self):
        q = super(AccessViewSet, self).get_queryset()
        user: User = self.request.user
        if user.is_staff:
            return q
        services: List[IntensiveCareService] = [
            a.intensive_care_service for a in user.admin_accesses]

        return q.filter(
            Q(intensive_care_service__in=services) | Q(user=user)
        )

    @action(detail=True, methods=['patch'], url_path="validate")
    def validate(self, *args, **kwargs):
        access: Access = self.get_object()
        access.validate()
        return Response(self.get_serializer(access).data,
                        status=status.HTTP_200_OK)

    @action(detail=True, methods=['patch'], url_path="deny")
    def deny(self, *args, **kwargs):
        access: Access = self.get_object()
        access.deny()
        return Response(self.get_serializer(access).data,
                        status=status.HTTP_200_OK)

    @action(detail=True, methods=['patch'], url_path="close")
    def close(self, *args, **kwargs):
        access: Access = self.get_object()
        access.close()
        return Response(self.get_serializer(access).data,
                        status=status.HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        raise MethodNotAllowed('patch')
