from rest_framework import permissions

from orea.models import User
from users.models import Access


class AccessPermissions(permissions.IsAuthenticated):
    def has_object_permission(self, request, view, obj: Access):
        user: User = request.user
        if request.method in ["DELETE"]:
            # user can only delete one of their accesses
            return (user.is_staff or obj.user.pk == user.pk)\
                and (obj.status != Access.Status.validated or not obj.started)
        return request.method in ["PATCH"]
