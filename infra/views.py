from django.db.models import Prefetch
from django_filters import filters
from drf_spectacular.utils import extend_schema, OpenApiParameter

from orea.models import User
from orea.permissions import IsAdminOrReadOnly
from orea.views import BaseViewSet, BaseFilterSet
from .models import IntensiveCareService, Hospital, Bed, Unit
from .serializers import IntensiveCareServiceSerializer, HospitalSerializer, \
    BedSerializer, NestedIntensiveCareServiceSerializer, UnitSerializer, \
    NestedUnitSerializer, NestedBedSerializer


class HospitalViewSet(BaseViewSet):
    serializer_class = HospitalSerializer
    queryset = Hospital.objects.all()
    permission_classes = (IsAdminOrReadOnly,)
    search_fields = ("name",)


class ServiceFilterSet(BaseFilterSet):
    class Meta:
        model = IntensiveCareService
        fields = ("hospital", "uuid")


class IntensiveCareServiceViewSet(BaseViewSet):
    queryset = IntensiveCareService.objects.all()
    serializer_class = IntensiveCareServiceSerializer
    nested_serializer_class = NestedIntensiveCareServiceSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filterset_class = ServiceFilterSet
    search_fields = ("name", "hospital__name")

    @extend_schema(
        parameters=[OpenApiParameter(name='full', type=bool,
                                     description="Getting nested result")])
    def list(self, request, *args, **kwargs):
        return super(BaseViewSet, self).list(request, *args, **kwargs)


class UnitFilterSet(BaseFilterSet):
    hospital = filters.CharFilter(field_name="intensive_care_service__hospital")

    class Meta:
        model = Unit
        fields = ("uuid", "intensive_care_service", "hospital")


class UnitViewSet(BaseViewSet):
    queryset = Unit.objects.all()
    serializer_class = UnitSerializer
    nested_serializer_class = NestedUnitSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filterset_class = UnitFilterSet
    search_fields = ("name", "intensive_care_service__name",
                     "intensive_care_service__hospital__name")

    def get_queryset(self):
        q = super(UnitViewSet, self).get_queryset()
        user: User = self.request.user
        if user.is_staff:
            return q
        authorized_reas = user.authorized_intensive_care_services.all()
        return super(UnitViewSet, self).get_queryset().filter(
            intensive_care_service__in=authorized_reas
        ).prefetch_related("beds")

    @extend_schema(
        parameters=[
            OpenApiParameter(name='full', type=bool,
                             description="Getting nested result"),
            OpenApiParameter(name='uuid', type=str,
                             description="',' separated uuids'"),
        ])
    def list(self, request, *args, **kwargs):
        return super(BaseViewSet, self).list(request, *args, **kwargs)


class BedFilterSet(BaseFilterSet):
    class Meta:
        model = Bed
        fields = ("uuid", "unit", "unit_index", "usable")


class BedViewSet(BaseViewSet):
    queryset = Bed.objects.all()
    serializer_class = BedSerializer
    nested_serializer_class = NestedBedSerializer
    permission_classes = (IsAdminOrReadOnly,)
    search_fields = ("unit__name", "unit__intensive_care_service__name",
                     "unit__intensive_care_service__hospital__name")
    filterset_class = BedFilterSet

    def get_queryset(self):
        q = super(BedViewSet, self).get_queryset()
        user: User = self.request.user
        if user.is_staff:
            return q
        authorized_reas = user.authorized_intensive_care_services.all()
        q = super(BedViewSet, self).get_queryset().filter(
            unit__intensive_care_service__in=authorized_reas
        )
        try:
            from patients.models import BedStay
            q = q.prefetch_related(Prefetch(
                "bed_stays",
                queryset=BedStay.objects.filter(BedStay.is_current_Q()),
                to_attr='prefetched_current_stays',
            ))
        except ImportError:
            pass
        return q

    @extend_schema(
        parameters=[OpenApiParameter(name='full', type=bool,
                                     description="Getting nested result")])
    def list(self, request, *args, **kwargs):
        return super(BaseViewSet, self).list(request, *args, **kwargs)
