import random
import uuid
from typing import List

from rest_framework import status

from orea.models import User
from orea.tests_tools import ViewSetTests, new_random_user,\
    CaseRetrieveFilter, random_str, ListCase, CreateCase, PatchCase, DeleteCase
from infra.models import IntensiveCareService as Service, Hospital
from infra.views import IntensiveCareServiceViewSet as ServiceViewSet

INFRA_URL = "/infra"


class ServiceTests(ViewSetTests):
    unupdatable_fields = []
    unsettable_default_fields = dict()
    unsettable_fields = []

    objects_url = f"{INFRA_URL}/services"
    retrieve_view = ServiceViewSet.as_view({'get': 'retrieve'})
    list_view = ServiceViewSet.as_view({'get': 'list'})
    create_view = ServiceViewSet.as_view({'post': 'create'})
    delete_view = ServiceViewSet.as_view({'delete': 'destroy'})
    update_view = ServiceViewSet.as_view({'patch': 'partial_update'})
    model = Service
    model_objects = Service.objects
    model_fields = Service._meta.fields

    def setUp(self):
        super(ServiceTests, self).setUp()

        # Users
        self.admin: User = new_random_user("is", "admin", is_staff=True)
        self.not_admin: User = new_random_user("not", "admin")

        # Hospitals
        self.hosp1: Hospital = Hospital.objects.create(name="hosp1")
        self.hosp2: Hospital = Hospital.objects.create(name="hosp2")


class ServiceCaseRetrieveFilter(CaseRetrieveFilter):
    def __init__(self, name: str, exclude: dict = None):
        self.name = name
        super(ServiceCaseRetrieveFilter, self).__init__(exclude=exclude)


class ServiceGetListTests(ServiceTests):
    def setUp(self):
        super(ServiceGetListTests, self).setUp()
        self.name_pattern = random_str(3).replace(' ', 'e')
        nb_services = 500

        self.service_names = [
                              random_str(random.randint(5, 15)) for _ in
                              range(nb_services - 110)
                          ] + [
                              random_str(random.randint(2, 7))
                              + self.name_pattern
                              + random_str(random.randint(3, 5)) for _ in
                              range(110)
                          ]

        self.list_services: List[Service] = Service.objects.bulk_create([
            Service(
                **{
                    'name': name,
                    'hospital': random.choice([self.hosp1, self.hosp2])
                }
            ) for name in self.service_names
        ])

    def test_get_all_services(self):
        # As a user with no right, I can get all services
        case = ListCase(
            to_find=self.list_services,
            success=True,
            status=status.HTTP_200_OK,
            user=self.not_admin
        )
        self.check_get_paged_list_case(case)

    def test_get_list_with_params(self):
        # As a user with no right, I can get services filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.not_admin)
        cases = [
            ListCase(
                **basic_case_dict,
                title=f"search={self.name_pattern}",
                to_find=[
                    service for service in self.list_services
                    if self.name_pattern in service.name],
                params=dict(search=self.name_pattern)
            ),
            ListCase(
                **basic_case_dict,
                title=f"search={self.hosp1.name}",
                to_find=[
                    service for service in self.list_services
                    if (
                            self.hosp1.name in service.name
                            or service.hospital.pk == self.hosp1.pk
                    )],
                params=dict(search=self.hosp1.name)
            ),
            ListCase(
                **basic_case_dict,
                title=f"hospital={self.hosp1.pk}",
                to_find=[
                    service for service in self.list_services
                    if service.hospital.pk == self.hosp1.pk
                ],
                params=dict(hospital=self.hosp1.pk)
            ),
        ]
        [self.check_get_paged_list_case(case) for case in cases]

    def test_error_not_authenticated(self):
        # As a user not authenticated, I cannot get any services
        case = ListCase(
            to_find=[],
            success=False,
            status=status.HTTP_401_UNAUTHORIZED
        )
        self.check_get_paged_list_case(case)


class ServiceCreateTests(ServiceTests):
    def setUp(self):
        super(ServiceCreateTests, self).setUp()

        test_service_name = random_str(10)
        self.creation_data = {
            'name': test_service_name,
            'hospital': str(self.hosp1.pk)
        }
        self.basic_create_case = CreateCase(
            data=self.creation_data,
            retrieve_filter=ServiceCaseRetrieveFilter(
                name=test_service_name.strip()
            ),
            user=None, status=None, success=None,
        )

    def test_create_as_admin(self):
        # As a user with is_staff=True, I can create a new service
        case = self.basic_create_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_201_CREATED,
        )
        self.check_create_case(case)

    def test_error_create_wrong_data(self):
        # As a user with is_staff=False, I cannot create a new service
        cases = [self.basic_create_case.clone(
            user=self.not_admin,
            data=data,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        ) for data in [
            {**self.creation_data, 'hospital': str(uuid.uuid4())}
        ]]
        [self.check_create_case(case) for case in cases]

    def test_error_create_as_basic_user(self):
        # As a user with is_staff=False, I cannot create a new service
        case = self.basic_create_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_create_case(case)


class ServicePatchTests(ServiceTests):
    def setUp(self):
        super(ServicePatchTests, self).setUp()

        self.created_data = {
            'name': 'created',
            'hospital': self.hosp1
        }
        self.base_data_to_update = {
            'name': 'updated',
            'hospital': str(self.hosp2.pk)
        }
        self.basic_patch_case = PatchCase(
            initial_data=self.created_data,
            data_to_update=self.base_data_to_update,
            user=None, status=None, success=None,
        )

    def test_patch_as_admin(self):
        # As a user with is_staff=True, I can edit a service
        case = self.basic_patch_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_200_OK,
        )
        self.check_patch_case(case)

    def test_error_patch_as_basic_user(self):
        # As a user with is_staff=False, I cannot edit a service
        case = self.basic_patch_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_patch_case(case)


class ServiceDeleteTests(ServiceTests):
    def setUp(self):
        super(ServiceDeleteTests, self).setUp()

        self.created_data = {
            'name': 'created',
            'hospital': self.hosp1
        }
        self.basic_delete_case = DeleteCase(
            data_to_delete=self.created_data,
            user=None, status=None, success=None,
        )

    # when we'll be safe with service deletion (cascade, deletion, etc.)
    def test_delete_user_as_main_admin(self):
        # As a user with is_staff=True, I can soft-delete a service
        case = self.basic_delete_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_204_NO_CONTENT,
        )
        self.check_delete_case(case)

    def test_error_delete_user_as_simple_user(self):
        # As a user with is_staff=False, I cannot delete a service
        case = self.basic_delete_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_delete_case(case)
