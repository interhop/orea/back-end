FROM python:3.10.8-slim-bullseye

WORKDIR /app

# Install needed dependencies
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && apt-get install -y \
    postgresql postgresql-contrib gcc libpq-dev \
    nginx curl gettext locales locales-all \
    xxd nano cron \
    && rm -rf /var/lib/apt/lists/*

# Install requirement for python
COPY ./requirements.txt ./
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install gunicorn

ENV LC_ALL="fr_FR.utf8"
ENV LC_CTYPE="fr_FR.utf8"
RUN dpkg-reconfigure locales

COPY . .

RUN chmod +x docker-entrypoint.sh

ENTRYPOINT ["/app/docker-entrypoint.sh"]
