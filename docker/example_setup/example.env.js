window.env = {
  "REACT_APP_DEV_MODE":"true",
  "REACT_APP_BASE_URL": "/api/",
  "REACT_APP_GITLAB_MAILTO": "gitlab-incoming+interhop-orea-issues-87295-issue-@framagit.org"
  "REACT_APP_DELAY_TOKEN_REFRESH_BEFORE_EXPIRATION": 600,
  "REACT_APP_INACTIVITY_DELAY_BEFORE_EXPIRATION": 300
}