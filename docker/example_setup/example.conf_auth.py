from typing import Union

from dj_rest_auth.utils import jwt_encode

from .models import User
from .settings import IS_LOCAL_SERVER, DEMO_USER_USERNAME
from .types import JwtTokens


def find_external_user(username: str) -> User:
    raise NotImplementedError


def check_ids(username: str, password: str) -> JwtTokens:
    """
    Verify a user credentials and return a connexion Jwt token
    raise
    Can raise .types.LoginError if ids are wrong or .types.ServerError
    if a problem occurs with the authentication server
    """
    user = User.objects.get(username=username)
    auto_valid = IS_LOCAL_SERVER or (
            DEMO_USER_USERNAME is not None and username != DEMO_USER_USERNAME
    )
    if not auto_valid:
        user.check_password(password)

    access_token, refresh_token = jwt_encode(user)
    return JwtTokens(access_token, refresh_token)


def validate_token(raw_token: str) -> str:
    """
    Validates an encoded JSON web token and returns a validated token
    wrapper object.
    Token returned will be used with get_user
    """
    raise NotImplementedError


def get_user(
        access_token: str, auth_method: str = ''
) -> Union[None, User]:
    """
    Given an access token returned by validate_token,
    returns the matching user
    """
    raise NotImplementedError


def refresh_jwt(refresh: str) -> JwtTokens:
    """
    Returns a new Jwt token bound to the initial user
    """
    raise NotImplementedError
