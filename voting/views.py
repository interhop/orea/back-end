import nltk
from django.http import QueryDict, Http404
from drf_spectacular.utils import extend_schema, OpenApiParameter
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from orea.models import User
from orea.settings import VOTING_ATTACHMENT_MAX_SIZE, VOTING_POST_LABELS, \
    VOTING_BASE_LABELS
from orea.views import BaseViewSet
from .serializers import IssuePostSerializer, ApiIssueSerializer
from .util import AttachmentReturned, post_gitlab_attachment,\
    post_gitlab_issue, get_issues, get_issue, patch_issue

nltk.data.path = ['./voting/nltk_data']
stopwords = (set(nltk.corpus.stopwords.words('english'))
             | set(nltk.corpus.stopwords.words('french')))


def build_error_message(status_code: int, msg: str) -> str:
    return f"Error {status_code} while contacting gitlab server : {msg}"


def build_full_description(description: str, user: User, markdown: str) -> str:
    return f"{description}\n\nEnvoyée par " \
           f"{user or 'Unknown'} (pour lui répondre: {user.email})" \
           f"\n\n*Pensez à effacer l'adresse email " \
           f"avant de duppliquer l'issue*" \
           + (
               f"\n\nAttachment: {markdown}\n\nPar sécurité, vérifier que vous "
               f"ayez un antivirus à jour avant tout téléchargement"
               if markdown else ""
           )


def vote_description(description: str, user: User) -> str:
    res = description
    if '/votes' not in description:
        res = f"{res}\n\n/votes"

    if f"- {user.uuid}" not in res.split("/votes")[-1]:
        res = f"{res}\n- {user.uuid}"
    return res


def unvote_description(description: str, user: User) -> str:
    if '/votes' not in description:
        return description

    if f"\n- {user.uuid}" not in description:
        return description

    return description.replace(f"\n- {user.uuid}", "")


class IssuePostViewSet(BaseViewSet):
    queryset = None
    permission_classes = (IsAuthenticated,)
    serializer_class = IssuePostSerializer
    parser_classes = (MultiPartParser,)
    http_method_names = ['post', 'get', 'patch']

    def partial_update(self, request, *args, **kwargs):
        raise MethodNotAllowed('partial_update')

    def retrieve(self, request, *args, **kwargs):
        raise MethodNotAllowed('retrieve')

    # @swagger_auto_schema(
    #     manual_parameters=[
    #         openapi.Parameter(
    #             "label", openapi.IN_FORM,
    #             type=openapi.TYPE_STRING,
    #             description=f"Can be one of "
    #                         f"{', '.join(VOTING_POST_LABELS)}"
    #         ),
    #     ],
    #     responses={
    #         201: openapi.Response('Success', ApiIssueSerializer),
    #         400: openapi.Response('Invalid data', ErrorSerializer),
    #         403: openapi.Response('Not authenticated'),
    #         500: openapi.Response('Error within connection to Gitlab server',
    #                               ErrorSerializer)
    #     },
    # )
    def create(self, request, *args, **kwargs):
        """
        Post a new issue. This issue is either a bug or a feature request, and
        will be added in corresponding columns in gitlab. The posted data must
        contain a single label, a title and a description.
        """
        for field in ['title', 'description']:
            if field not in request.data:
                return Response(
                    {'error': f'missing {field} in the POST request'},
                    status=status.HTTP_400_BAD_REQUEST
                )

        title = request.data['title']
        description = request.data['description']
        is_incident = request.data.get('isIncident', "false") == "true"
        label = request.data.get('label', None)
        attach = request.data.get('attachment', None)
        markdown = ""

        if label is not None and label not in VOTING_POST_LABELS:
            return Response(
                {
                    'message': f"label '{label}' not authorized, choices are: "
                               f"{','.join(VOTING_POST_LABELS)}"
                },
                status=status.HTTP_400_BAD_REQUEST
            )

        if len(description) == 0:
            return Response({'message': 'title ou description empty'},
                            status=status.HTTP_400_BAD_REQUEST)
        if len(title) == 0:
            return Response({'message': 'title ou description empty'},
                            status=status.HTTP_400_BAD_REQUEST)
        if attach:
            if attach.size > VOTING_ATTACHMENT_MAX_SIZE:  # 10Mo
                return Response({
                    'message': f"Le fichier joint doit être de taille "
                               f"inférieure à "
                               f"{VOTING_ATTACHMENT_MAX_SIZE / 10 ** 6}Mo"
                }, status=status.HTTP_400_BAD_REQUEST
                )
            attach_res = post_gitlab_attachment(file=attach)
            if attach_res.status_code != status.HTTP_201_CREATED:
                return Response({
                    "message": build_error_message(
                        attach_res.status_code, attach_res.text
                    ),
                }, status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )
            gitlab_attachment = AttachmentReturned(resp=attach_res)
            markdown = gitlab_attachment.markdown

        res = post_gitlab_issue(data={
            'title': title,
            'description': build_full_description(
                description=description, user=request.user, markdown=markdown
            ),
            'labels': VOTING_BASE_LABELS + ([label] if label else []),
            'issue_type': 'incident' if is_incident else 'issue'
        })
        if res.status_code != 201:
            return Response({
                "message": build_error_message(res.status_code, res.text),
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

        return Response(
            ApiIssueSerializer(res.json()).data, status=status.HTTP_201_CREATED
        )

    @extend_schema(
        parameters=[
            OpenApiParameter(name='labels', type=bool,
                             description="Getting nested result")])
    def list(self, request, *args, **kwargs):
        if 'search' in request.GET:
            if type(request.GET) == QueryDict:
                request.GET._mutable = True
            request.GET['search'] = ','.join([
                w for w in request.GET.get('search').split(',')
                if w not in stopwords
            ])
        issues = ApiIssueSerializer(get_issues(request.GET), many=True)
        return Response(data={
            'results': issues.data
        }, status=status.HTTP_200_OK)

    @action(detail=True, methods=['patch'], url_path="vote")
    def vote(self, *args, **kwargs):
        user: User = self.request.user
        project_id, issue_id = self.kwargs.get(self.lookup_field, '').split('-')
        if not project_id or not issue_id:
            raise Http404("Required to enter id following 'projectId-issueIid'")

        issue = get_issue(project_id, issue_id)
        issue = ApiIssueSerializer(issue).data
        issue['description'] = vote_description(
            issue.get('description', ""), user)
        new_issue = patch_issue(issue)

        return Response(ApiIssueSerializer(new_issue).data,
                        status=status.HTTP_200_OK)

    @action(detail=True, methods=['patch'], url_path="unvote")
    def unvote(self, *args, **kwargs):
        user: User = self.request.user
        project_id, issue_id = self.kwargs.get(self.lookup_field, '').split('-')
        if not project_id or not issue_id:
            raise Http404("Required to enter id following 'projectId-issueIid'")

        issue = get_issue(project_id, issue_id)
        issue = ApiIssueSerializer(issue).data
        issue['description'] = unvote_description(
            issue.get('original_description', ""), user)
        new_issue = patch_issue(issue)

        return Response(ApiIssueSerializer(new_issue).data,
                        status=status.HTTP_200_OK)
