from django.urls import path, include

from orea.urls import OptionalSlashRouter
from . import views

router = OptionalSlashRouter(trailing_slash=False)
router.register(r'issues', views.IssuePostViewSet, basename="issues")

urlpatterns = [
    path('', include(router.urls)),
]
